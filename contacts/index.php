<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контактная информация");
?>
<div class="regular-page-box">
		<div class="container">
			<h1>Контакты</h1>
			
			<div class="contacts-wrapper">
				<div class="container-1005">
					<div class="contacts-common-email"><span>Общий e-mail:</span> <a href="mailto:info@kartsup.ru">info@kartsup.ru</a></div>
					<div class="contacts-managers-items row">
						<div class="col-md-6 col-sm-6 col-xs-12 contacts-manager-col">
							<div class="contacts-manager-item">
								<div class="contacts-manager-item__photo" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/manager-photo-1.jpg');"></div>
								<div class="contacts-manager-item__content">
									<div class="contacts-manager-item__name">Генеральный директор<br> Кураков Артур Юрьевич</div>
									<div class="contacts-manager-item__list">
										<span><i>тел.</i> <a href="tel:+79109409976">+7 (910) 940-99-76</a></span>
										<span><i>e-mail:</i> <a class="email" href="mailto:kart@kartsup.ru">kart@kartsup.ru</a></span>
										<span><i>Skype:</i> <a href="skype:kart184?call">kart184</a></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 contacts-manager-col">
							<div class="contacts-manager-item">
								<div class="contacts-manager-item__photo" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/manager-photo-2.jpg');"></div>
								<div class="contacts-manager-item__content">
									<div class="contacts-manager-item__name">Заместитель генерального директора<br> Павлов Вадим Геннадиевич </div>
									<div class="contacts-manager-item__list">
										<span><i>тел.</i> <a href="tel:+79038443648">+7 (903) 844-36-48</a></span>
										<span><i>e-mail:</i> <a class="email" href="mailto:pawadim@kartsup.ru">pawadim@kartsup.ru</a></span>
										<span><i>Skype:</i> <a href="skype:voldemarias?call">voldemarias</a></span>
										<span><i>ICQ:</i> 308969062</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="contacts-work-info">Работаем по Центральному федеральному округу Российской Федерации. Разработка программного обеспечения возможна для клиента из любой части света.</div>
					<div class="contacts-feedback">
						<form class="contacts-feedback-form row" action="#" name="feedback">
							<div class="col-md-6 col-sm-12 col-xs-12 contacts-feedback-form-col">
								<div class="input-box">
									<input type="text" name="name" placeholder="* Ваше имя" required>
								</div>
								<div class="input-box">
									<input type="email" name="email" placeholder="* E-mail" required>
								</div>
								<div class="input-box">
									<input class="phone-mask" type="tel" name="phone" placeholder="* Номер телефона" required>
								</div>
							</div>
							<div class="col-md-6 col-sm-12 col-xs-12 contacts-feedback-form-col">
								<textarea name="comment" placeholder="* Ваше сообщение" required></textarea>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 submit-box">
								<button class="arrow-btn dark-blue-btn" type="submit">Отправить</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>