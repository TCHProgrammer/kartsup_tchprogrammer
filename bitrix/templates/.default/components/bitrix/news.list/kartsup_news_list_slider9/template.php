<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="main-news-section">
		<div class="container">
			<div class="main-news-box">
				<h2>Новости</h2>
				<div class="main-news-list">
					<?$counter = 1;?>
					<?foreach($arResult["ITEMS"] as $arItem):?>
							<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>


							<a class="main-news-item news-item-<?=$counter?> main-news" href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-ajax-element-id="<?echo $arItem["ID"]?>">
								<div class="main-news-item__bg" style="background-image: url('<?=SITE_TEMPLATE_PATH."/pic/main-news-pic-$counter.jpg"?>');"></div>
								<div class="main-news-item__content">
									<div class="main-news-item__caption"><span><?echo $arItem["NAME"]?></span></div>
									<div class="main-news-item__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
								</div>
							</a>


						<?$counter++;?>
						<?endforeach?>


				</div>
				<div class="main-news-more"><a class="arrow-btn" href="/news/"><span>Смотреть все новости</span><i></i></a></div>
			</div>
		</div>
	</section>


