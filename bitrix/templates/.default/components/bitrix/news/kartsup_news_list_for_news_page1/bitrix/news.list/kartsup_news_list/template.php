<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-wrapper">

				<div class="news-list-box">
					<div class="news-list to-ajax-news-list" data-ajax-element-id="<?echo $arItem["ID"]?>">
						<?foreach($arResult["ITEMS"] as $arItem):?>
							<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>
							<a class="news-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-ajax-element-id="<?echo $arItem["ID"]?>">
								<div class="news-item__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
								<div class="news-item__caption"><?echo $arItem["NAME"]?></div>
								<div class="news-item__preview"><?echo $arItem["PREVIEW_TEXT"];?></div>
							</a>
						<?endforeach?>

					</div>
					<div class="news-list-more-btn"><a class="arrow-btn" href="/news/news_ajax.php?PAGE_NUMBER=2">Больше новостей</a></div>
				</div>
</div>
<script type="text/javascript">

			var news_count = <?=$arParams['NEWS_COUNT']?>;

			var moreBtnClick = function(e){
				e.preventDefault();
				var curr_href = $('.news-list-more-btn a').eq(0).attr('href');
				var split_href1 = curr_href.split('?');
				var split_href2 = split_href1[1].split('=');
				var curr_page_number = split_href2[1];

				if(!curr_page_number){
					curr_page_number = 3;
				}





				 BX.showWait();

				BX.ajax({   
                	url: curr_href,
					data: {
						page_size:news_count
					},
					method: 'POST',
                	dataType: 'html',
					timeout: 30,
					async: true,
					processData: true,
					scriptsRunFirst: true,
					emulateOnload: true,
					start: true,
                	cache: false,
                	onsuccess: function(data){
						$('.to-ajax-news-list').append(data);


						if($('.stop_ajax_pager_indicator').length){
							$('.news-list-more-btn').remove();
							$('.stop_ajax_pager_indicator').remove();
							BX.closeWait();
							return true;
						}


						var next_page_number = parseInt(curr_page_number) + 1;

						var next_href = '/news/news_ajax.php?PAGE_NUMBER='+next_page_number;
						


						$('.news-list-more-btn a').attr('href','/news/news_ajax.php?PAGE_NUMBER='+(parseInt(next_page_number)));
						BX.closeWait();
                	},
                	onfailure: function(error){
                    	console.info(error);
               	 	}
            	});
			}


			$('.news-list-more-btn').click(moreBtnClick);

		</script>