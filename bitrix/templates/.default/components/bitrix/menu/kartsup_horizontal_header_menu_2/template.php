<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>


<div class="swiper-wrapper">

						<?foreach($arResult as $arItem):?>

								<div class="swiper-slide">
									<a class="products-head-item product-1 <?=$arItem["SELECTED"]?'current':''?>" href="<?=$arItem["LINK"]?>">
										<div class="products-head-item__caption">REVIT</div>
										<div class="products-head-item__about"><?=$arItem["TEXT"]?></div>
									</a>
								</div>






						<?endforeach?>


						<!--
						<div class="swiper-slide">
							<a class="products-head-item product-1 current" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Копирование параметров</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-2" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Дополнение редактирования</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-3" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Менеджер листов</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-4" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Рабочая плоскость</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-5" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Дополнение Переименовка</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-6" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Revit UI reset</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-7" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Суперфильтр</div>
							</a>
						</div>
						-->
</div>



<?endif?>


