<?
define('NEED_TO_CHARSET_CONVERT',true);

class downloads{
	
	/*
		$sort = 'ASC','DESC', false
	*/
	public static function filesArrayFromDir($dir_from_site_root,$sort='ASC'){
		$dir = opendir($_SERVER['DOCUMENT_ROOT'].$dir_from_site_root);


		$files_array = array();
		
		/*
		if(!is_dir($dir)){
			return $files_array;
		}
		*/
		
		
		
   		while($file = readdir($dir))
    	{
			if($file == '..' || $file == '.' || $file == '.section.php'){
				continue;
			}
			if(NEED_TO_CHARSET_CONVERT === true){
				$files_array[] = iconv("cp1251", "UTF-8", $file);
			} else {
				$files_array[] = $file;
			}
  		}
								
		if($sort && $sort=='ASC'){
			sort($files_array);
		} else if($sort && $sort=='DESC'){
			rsort($files_array);
		}
		
		return $files_array;
	}
	
	
	
	public static function extractNameFromFileName($fileName){
		$exp = explode('.',$fileName);
		array_pop($exp);
		
		$newstr = implode($exp);
		
		$exp = explode('/',$newstr);
		
		return array_pop($exp);
	}

	public static function getFileExtension($filename) {
    	return end(explode(".", $filename));
  	}
	
	
	
	
	public static function deleteNotExistingFilesFromArray($files_array,$dir_from_site_root){
		
		foreach($files_array as $k=>$file){
		
			if(NEED_TO_CHARSET_CONVERT){
				$path = iconv("UTF-8", "cp1251", $_SERVER['DOCUMENT_ROOT'].$dir_from_site_root.'/'.$file);
			} else {
				$path = $_SERVER['DOCUMENT_ROOT'].$dir_from_site_root.'/'.$file;
			}
			
			
		
			if(!file_exists($path)){
				unset($files_array[$k]);
				return self::deleteNotExistingFilesFromArray($files_array, $dir_from_site_root);
			}
		}
		return $files_array;
		
	}

	public static function writeDownloadReferences($dir_from_site_root, $files_array){
		foreach($files_array as $file){
				
				echo '<a class="arrow-btn download-btn" href="'.$dir_from_site_root.'/'.$file.'" download="'.basename($file).'"><span>'.self::extractNameFromFileName($file).'</span><s></s></a>';
		}
	}
	
	public static function displayDownloadReferences($dir_from_site_root,$sort='ASC'){
		
		$files_array = self::filesArrayFromDir($dir_from_site_root,$sort);
		self::writeDownloadReferences($dir_from_site_root, $files_array);
	}
	
	public static function displayDownloadReferencesFromArray($dir_from_site_root,$files_array){
		
		$files_array = self::deleteNotExistingFilesFromArray($files_array,$dir_from_site_root);
		self::writeDownloadReferences($dir_from_site_root, $files_array);
		
	}
	
	public static function getFileNamesFromNews($news_id){
		
		$result = array();
		
		$arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_FILES_TO_DOWNLOAD");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 'ID'=>$news_id);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
		if($ob = $res->GetNextElement()){ 
			//$arFields = $ob->GetFields();  
			//print_r($arFields);
			$arProps = $ob->GetProperties();
			
			foreach($arProps['FILES_TO_DOWNLOAD']['VALUE'] as $file){
				$result[] = $file;
			}
			
			
		}
		
		
		return $result;
		
	}
	
	
	public static function getFileNamesAndDescriptionsFromNews($news_id){
		$result = array();
		
		$arSelect = Array("IBLOCK_ID", "ID", "PROPERTY_FILES_TO_DOWNLOAD");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 'ID'=>$news_id);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
		if($ob = $res->GetNextElement()){ 
			//$arFields = $ob->GetFields();  
			//print_r($arFields);
			$arProps = $ob->GetProperties();
			
			
			
			foreach($arProps['FILES_TO_DOWNLOAD_FROM_COMPUTER']['VALUE'] as $file){
				$file_info = CFIle::GetById($file);
				$file_desc = $file_info->Fetch()['DESCRIPTION'];
				$file_path = CFile::GetPath($file);
				
				$result[] = array('DESCRIPTION'=>$file_desc, 'PATH'=>$file_path);
			}
		}
		
		
		return $result;
	}
	
	public static function getRealFilePathsFromNews($news_id, $default_dir='downloads/news'){
		$files = self::getFileNamesFromNews($news_id);
		
		$result = array();
		
		foreach($files as $file){
			if(strpos($file,'/') === '0'){
				$result[] = $file;
			} else {
				$result[] = $default_dir.'/'.$file;
			}
		}
		
		
		
		return $result;
	}
	
	public static function displayDownloadReferencesFromNewsId($news_id, $default_dir='downloads/news'){
		define('NEED_TO_CHARSET_CONVERT',false);
		$realFilePaths = self::getRealFilePathsFromNews($news_id, $default_dir);
		self::displayDownloadReferencesFromArray('',$realFilePaths);
	}
	
	public static function displayDownloadReferencesFromNewsFiles($news_id){
		$files = self::getFileNamesAndDescriptionsFromNews($news_id);
		foreach($files as $file){
				
			echo '<a class="arrow-btn download-btn" href="'.$file['PATH'].'" download="'.$file['DESCRIPTION'].'.'.self::getFileExtension($file['PATH']).'"><span>'.$file['DESCRIPTION'].'</span><s></s></a>';
		}
	}
								
}


?>
