<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Информация о компании");

?>



	

	<div class="regular-page-box">

		<div class="container">

			<h1>О компании</h1>

			

			<div class="bim-management-wrapper about-company-wrapper">

				<div class="bim-management-info">

					<p>В современном мире компьютерные технологии развиваются стремительно и все большее влияние оказывают

						на те отрасли в которых ранее казались не востребованы. Лет 10 назад человек не знающий компьютер не испытывал проблем с поиском работы, а знание компьютера не являлось конкурентным преимуществом. Сейчас же на каждом углу можно увидеть  монитор вместо записной книжки или рабочего места. Компьютер выполняет за человека механическую работу и даже более. Он призван облегчить нам жизнь, но для того чтобы это случилось его нужно приручить

						или обучить чему то новому. Этот процесс требует вдумчивого подхода к решению данной задачи и мы готовы взять

						эту ответственность на себя. С нами вы будете ставить задачи, которые вам требуется решать а мы уже будем думать как их автоматизировать с минимальными трудозатратами со стороны пользователя не забывая о удобстве работы

						с конечным продуктом.</p>

					<p>Наша компания занимается комплексным обслуживанием организаций с точки зрения информационных технологий.<br> В перечень наших услуг входит:</p>

				</div>


				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider4", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "ABOUT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider4"
	),
	false
);?>

				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_popup_vertical_long_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_popup_vertical_long_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "ABOUT",
		"IBLOCK_ID" => "5",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>
				<!--
				<div class="services-toggle-items">

					

					<div class="services-toggle-item wow fadeInUp">

						

						<div class="services-toggle-item-head">

							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-pic-1.jpg');"></div>

							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-bg.jpg');">

								<div class="services-toggle-item-head__caption">Разработка программного обеспечения</div>

								<a class="more-btn white-btn toggle-btn" href="#"></a>

							</div>

						</div>

						

						<div class="services-toggle-item-body">

							<div class="services-toggle-item-body__box b-content">

								<p>Мы не ограничиваем себя рамками одной отрасли и стараемся изучать все новое. На данном этапе мы плотно работаем над такими направлениями как:</p>

								<ul>

									<li><strong>базы данных,</strong><br> связанными с вашей деятельностью в любое время и в любом месте или вам  нужно предоставить доступ и управлять данными по определенным  алгоритмам, или любые данные которые нужно структурировать, изменять,  получать по определенным условиям в любой момент времени)

									

									<li><strong>3D представление и навигация,</strong><br>

									(Создание виртуального гида, или интерактивной презентации)

									

									<li><strong>визуализация,</strong><br>

									(Создание виртуального гида, или интерактивной презентации)

									

									<li><strong>разработки для помощи в проектировании (дополнительные модули для программного продукта Revit).</strong>

								</ul>

								<p>Последнее направление для нас особенно ценно так как разработчики дополнений под эти продукты являются еще и пользователями данного ПО. Так что они могут не только разработать то, что вам требуется, но и посоветовать, как это лучше реализовать для удобства использования!</p>

							</div>

						</div>

						

					</div>

					

					<div class="services-toggle-item wow fadeInUp">

						

						<div class="services-toggle-item-head">

							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-pic-2.jpg');"></div>

							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-bg.jpg');">

								<div class="services-toggle-item-head__caption">Удалённое администрирование всех информационных систем</div>

								<a class="more-btn white-btn toggle-btn" href="#"></a>

							</div>

						</div>

						

						<div class="services-toggle-item-body">

							<div class="services-toggle-item-body__box b-content">

								<ul>

									<li>Интернет-шлюз, для организации, распределения и контроля доступа в Интернет для сотрудников.

									

									<li>Контролер домена + DNS-сервер, для распределения прав доступа к различным ресурсам локальной вычислительной сети.

									

									<li>DHCP-сервер, для организации автоматической настройки и получения адреса при  подключении новых устройств к локальной вычислительной сети.

									

									<li>Системы виртуализации, для оптимального распределения информационных  ресурсов.

									

									<li>Системы резервного копирования данных, для повышения надёжности  автоматизированной системы в кризисных ситуациях.

									

									<li>VPN-Сервер, для расширения локальной вычислительной сети путём объединения  в единое информационное пространство нескольких офисов, возможно расположенных по всему миру (возможности не ограничены).

									

									<li>Файловый сервер, для организации и упорядочивания хранения данных, необходимых для функционирования предприятия.

									

									<li>Сервер антивирусной защиты.

									

									<li>Почтовый сервер + антиспам фильтр.

									

									<li>IP-телефония.

									

									<li>Система видео наблюдения.

									

									<li>Сервер 1С.

								</ul>

							</div>

						</div>

						

					</div>

					

					<div class="services-toggle-item wow fadeInUp">

						

						<div class="services-toggle-item-head">

							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-pic-3.jpg');"></div>

							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/about-content-bg.jpg');">

								<div class="services-toggle-item-head__caption">BIM-менеджмент для проектных организаций</div>

								<a class="more-btn white-btn toggle-btn" href="#"></a>

							</div>

						</div>

						

						<div class="services-toggle-item-body">

							<div class="services-toggle-item-body__box b-content">

								<p><strong>BIM</strong> (Building Information Model) - это новый подход к проектированию, в котором здание рассматривается как единый объект, каждый из элементов которого обладает влияющими друг на друга и на объект в целом параметрами и свойствами. <strong>BIM</strong>  — это не конкретное программное обеспечение, а технология проектирования в которой за основу берётся не плоскостной чертёж, а 3D-модель. Для более эффективного перехода на данную технологию необходимо ввести дополнительного высокооплачиваемого специалиста в штат организации — <strong>BIM</strong>-менеджера. Воспользовавшись услугами нашей компании по <strong>BIM</strong>-менеджменту, Вы кроме экономии на высокоплачиваемом специалисте, получаете опыт нескольких компаний по внедрению данной технологии. В удалённый <strong>BIM</strong>-менеджмент входит:</p>

								<ul>

									<li>Разработка Стандарта Предприятия (Создание регламента работы, шаблонов,   пространства наименований, структуры папок)

									<li>Обучение сотрудников технологии BIM и внедрение Стандарта Предприятия.

									<li>Организация работы над проектом в программном обеспечении. Данная услуга  оказывается в течении всего жизненного цикла проекта и включает в себя:   ежедневный удаленный просмотр проекта на предмет соблюдения регламента 		  предприятия, обсуждение и консультирование по структуре проекта и внесения 		  изменений с точки зрения работы в специализированном программном 		  обеспечении, создание начальной структуры папок и файлов в соответствии с регламентом предприятия и организации совместного доступа с распределением прав.

								</ul>

							</div>

						</div>

						

					</div>

					

				</div>
				-->

				<?$APPLICATION->IncludeComponent("bitrix:news.list", "kartsup_news_list_slider5", Array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "3",	// Код информационного блока
		"IBLOCK_TYPE" => "index_page",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "10000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1"
	),
	false
);?>

				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_software_square_linear1", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_software_square_linear1",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "3",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>
				<!--
				<section class="products-slider-section">

					<h2>Программные продукты</h2>

					<div class="products-slider-box">

						<div id="productsSlider" class="products-slider swiper-container">

							<div class="swiper-wrapper">

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Копирование параметров</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png 1x, pic/product-icon-3@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Менеджер листов</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT UI reset</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Суперфильтр</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

							</div>

						</div>

						<div class="products-slider-controls">

							<div class="prev-project-slide"></div>

							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>

							<div class="next-project-slide"></div>

						</div>

					</div>

				</section>
				-->
				

			</div>

			

		</div>

	</div>
<script type="text/javascript">
	$('.services-toggle-item-body').css('display','block');
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>