<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Портфолио");
?>

<div class="regular-page-box">
		<div class="container">
			<h1>Портфолио</h1>
			
			<div class="bim-management-wrapper portfolio-wrapper">
				<div class="bim-management-info">
					<p>Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста для того, чтобы сделать ударение не на содержание, а на элементы дизайна. Такой текст также называется как заполнитель. Это очень удобный инструмент для моделей (макетов). Он помогает выделить визуальные элементы в документе или презентации, например текст, шрифт или разметка. Lorem ipsum по большей части является элементом латинского текста классического автора и философа Цицерона. Слова и буквы были заменены добавлением или сокращением элементов, поэтому будет совсем неразумно пытаться передать содержание; это не гениально, не правильно, используется даже не понятный латинский. Хотя Lorem ipsum напоминает классический латинский, вы не найдете никакого смысла в сказанном. Поскольку текст Цицерона не содержит буквы K, W, или Z,
						что чуждо для латинского, эти буквы, а также многие другие часто вставлены в случайном порядке, чтобы скопировать тексты различных Европейских языков, поскольку диграфы не встречаются в оригинальных текстах.</p>
					<p>В профессиональной сфере часто случается так, что личные или корпоративные клиенты заказывают, чтобы публикация была сделана и представлена еще тогда, когда фактическое содержание все еще не готово. </p>
				</div>

				<?$APPLICATION->IncludeComponent("bitrix:news.list", "kartsup_news_list_slider6", Array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "7",	// Код информационного блока
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "10000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1"
	),
	false
);?>

				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_vertical_popup_wide_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_vertical_popup_wide_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "1000",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"IBLOCK_ID" => "7",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>

				<!--
				<div class="services-toggle-items">
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic"><img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-big-1.png" alt=""></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/portfolio-content-bg-1.jpg');">
								<div class="services-toggle-item-head__caption">Русская промышленная компания</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda atque consectetur dolores doloribus est explicabo facere incidunt, nam nobis nostrum quibusdam quos, similique sint temporibus ullam veniam, veritatis voluptas.</p>
								<ul>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea facilis iusto laudantium, odit perspiciatis quam quis quisquam tempore ut! Consequuntur deleniti eius iure nemo perspiciatis quasi reprehenderit, rerum sit sunt.</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea facilis iusto laudantium, odit perspiciatis quam quis quisquam tempore ut! Consequuntur deleniti eius iure nemo perspiciatis quasi reprehenderit, rerum sit sunt.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda cupiditate dicta eum maiores repellendus suscipit ut! Blanditiis dolorem, hic id iure maiores, molestias odit sint tempora ullam velit voluptates?</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic"><img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-big-2.png" alt=""></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/portfolio-content-bg-2.jpg');">
								<div class="services-toggle-item-head__caption">ООО "Салигрэм"</div>
								<div class="services-toggle-item-head__note">BIM-менеджмент</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet asperiores blanditiis cum dolor, excepturi explicabo in incidunt minus nam natus nobis odit. Atque aut consequuntur culpa natus optio sequi soluta.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam, at dicta eaque ex iusto laborum, modi numquam odio provident quasi quod rerum velit. Asperiores minima molestiae neque quasi quo tenetur?</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic"><img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-big-3.png" alt=""></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/portfolio-content-bg-3.jpg');">
								<div class="services-toggle-item-head__caption">Ventsoft</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum maxime mollitia nihil numquam quidem repellendus, saepe temporibus veritatis voluptas. Accusamus, dignissimos eaque earum esse itaque iure possimus unde voluptate? Voluptatibus?</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic"><img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-big-4.png" alt=""></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/portfolio-content-bg-4.jpg');">
								<div class="services-toggle-item-head__caption">Архитектурная студия МIV</div>
								<div class="services-toggle-item-head__note">BIM-менеджмент</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<ul>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus architecto asperiores aut consectetur delectus exercitationem, hic in iste molestiae molestias nulla numquam obcaecati praesentium quas quisquam quo, totam vitae!</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus architecto asperiores aut consectetur delectus exercitationem, hic in iste molestiae molestias nulla numquam obcaecati praesentium quas quisquam quo, totam vitae!</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus architecto asperiores aut consectetur delectus exercitationem, hic in iste molestiae molestias nulla numquam obcaecati praesentium quas quisquam quo, totam vitae!</li>
								</ul>
							</div>
						</div>
					
					</div>
					
				</div>

				-->


<?$APPLICATION->IncludeComponent("bitrix:news.list", "kartsup_news_list_slider5", Array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "3",	// Код информационного блока
		"IBLOCK_TYPE" => "index_page",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "10000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1"
	),
	false
);?>


				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_software_square_linear1", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_software_square_linear1",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "3",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>

<!--

				<section class="products-slider-section">
					<h2>Программные продукты</h2>
					<div class="products-slider-box">
						<div id="productsSlider" class="products-slider swiper-container">


							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Копирование параметров</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, pic/product-icon-2@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-3.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-3@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Менеджер листов</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT UI reset</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Суперфильтр</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
							</div>

						</div>
						<div class="products-slider-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</section>

-->
				
				
			</div>
			
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

