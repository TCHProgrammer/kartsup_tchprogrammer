<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Удалённый BIM-менеджмент для проектных организаций");
?>

<div class="regular-page-box">
		<div class="container">
			<h1>Удалённый BIM-менеджмент для проектных организаций</h1>
			
			<div class="bim-management-wrapper">
				<div class="bim-management-info">
					<p><strong>BIM</strong> (Building Information Model) – это новый подход к проектированию, в котором здание рассматривается как единый объект, каждый из элементов которого обладает влияющими друг на друга и на объект в целом параметрами и свойствами. <strong>BIM</strong> – это не конкретное программное обеспечение, а технология проектирования в которой за основу берётся не плоскостной чертёж, а 3D-модель. Для более эффективного перехода на данную технологию необходимо ввести дополнительного высокооплачиваемого специалиста в штат организации – <strong>BIM</strong>-менеджера.</p>
					<p>Воспользовавшись нашими услугами по <strong>BIM</strong>-менеджменту, Вы получаете опыт нескольких компаний по внедрению данной технологии. Воспитать собственного <strong>BIM</strong>-менеджера нашего уровня из проектировщика возможно только спустя годы. Тем более, проектировщик должен заниматься проектированием, а надзор за технической частью и взаимосвязью всех  разделов лучше возложить на профессионалов. Кроме того, мы поддерживаем актуальность используемых технологий, следим за развитием новых, инновационных технических решений и предлагаем их использование на Вашем предприятии. В удалённый <strong>BIM</strong>-менеджмент входит:</p>
				</div>


		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider4", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider4"
	),
	false
);?>


				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_popup_vertical_long_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_popup_vertical_long_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"IBLOCK_ID" => "6",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>



				<!--
				<div class="services-toggle-items">
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-pic-1.jpg');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-bg-1.jpg');">
								<div class="services-toggle-item-head__caption">Разработка Стандарта Предприятия</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Оказание услуг по <strong>BIM</strong>-менеджменту всегда должны начинаться с создания Стандарта Предприятия, которое включает в себя:</p>
								<ul>
									<li>Разработку и подготовку документации, а также свода правил, по которым будет вестись работа над <strong>BIM</strong>-проектом.</li>
									<li>Разработку Регламента взаимодействия Руководства организации, Проектировщиков и <strong>BIM</strong>-менеджеров при совместной работе над Проектом.</li>
									<li>Регламентирование структуры папок Проекта.</li>
									<li>Создание шаблонов для различного программного обеспечения.</li>
									<li>Формирования пространства наименований (файлов, семейств и т.д.) </li>
								</ul>
								<p>Стандарт Предприятия может изменяться и дополняться с течением времени.</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-pic-2.jpg');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-bg-2.jpg');">
								<div class="services-toggle-item-head__caption">Обучение сотрудников Предприятия</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Обучение Ваших сотрудников методологии <strong>BIM</strong> проводится в течение двух-трёх месяцев, в зависимости от изначального уровня их подготовки.</p>
								<p>Начинается через один месяц после начала разработки Стандарта и включает в себя как обучение проектированию в Autodesk Revit, так и работе в рамках Стандарта Предприятия. Обучение состоит из лекционных и практических занятий, проводится очно с использованием презентационного материала. График обучения договорной. </p>
								<p>В процессе обучения мы научим Вас совместной работе в единой Модели, а также создавать сложные Модели (Проекты) в которых различные разделы взаимоувязаны:</p>
								<ul>
									<li>С нами у Вас получится создавать Проекты любого уровня сложности.</li>
									<li>В полуавтоматическом режиме моделировать стропильные системы и получать документацию и спецификации.</li>
									<li>В полуавтоматическом режиме проектировать армирование.</li>
									<li>Создавать узлы высокой детализации и получать документацию по ним.</li>
									<li>Создавать взаимоувязанные инженерные системы с проверкой на коллизии и пересечения.</li>
									<li>Проектировать необходимые параметрические 3D-модели (семейства Revit) в случае необходимости.</li>
								</ul>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-pic-3.jpg');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-bg-3.jpg');">
								<div class="services-toggle-item-head__caption">Организация работы над проектом в программном обеспечении</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Организация работ над проектом в программном обеспечении начинается со старта Проекта по технологии <strong>BIM</strong> и проводится постоянно: её результатом является непрерывная, стабильная работа в следующем специализированном программном обеспечении:</p>
								<ul>
									<li>Autodesk AutoCad,</li>
									<li>Autodesk Civil3D,</li>
									<li>Autodesk Revit,</li>
									<li>Autodesk Navisworks,</li>
									<li>ArchiCad,</li>
									<li>Microsoft Office,</li>
									<li>Autodesk 3ds Max.</li>
								</ul>
								<p>Консалтинг возможен по следующим разделам:</p>
								<ul>
									<li>АР,</li>
									<li>КЖ,</li>
									<li>ИОС (ОВ, ВК, ЭОМ),</li>
									<li>Autodesk Navisworks,</li>
									<li>ArchiCad,</li>
									<li>Microsoft Office,</li>
									<li>Autodesk 3ds Max.</li>
								</ul>
								<p>Старт Проекта возможен через месяц после начала Обучения. В Организацию работ над проектом в программном обеспечении входит:</p>
								<ul>
									<li>Ежедневный удаленный просмотр проекта на предмет соблюдения Стандарта предприятия.</li>
									<li>Обсуждение и консультирование по структуре проекта и внесение изменений с очки зрения работы в программном обеспечении.</li>
									<li>Создание начальной структуры папок и файлов в соответствии со Стандартом Предприятия и организация совместного доступа с распределением прав.</li>
								</ul>
								<p>Кроме того, в рамках данной услуги мы предоставляем разработанное нами специализированное программное обеспечение, позволяющее упростить различные рабочие процессы. А также проводим автоматизированный удаленный мониторинг действий проектировщиков.</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-pic-4.jpg');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-bg-4.jpg');">
								<div class="services-toggle-item-head__caption">Консультации <nobr>ИТ-отдела</nobr> Предприятия</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>Для внедрения методологии <strong>BIM</strong> в процесс проектирования, необходимо четкое понимание ИТ-отдела задач и схемы работы проектной компании в рамках данной технологии. Требуется изменения файловой структуре, доступа к файлам и папкам, внедрение дополнительных технологий. Не все ИТ-специалисты готовы к таким глобальным изменениям и им также может потребоваться дополнительное обучение и консультации в этом направлении.</p>
							</div>
						</div>
						
					</div>
					
					<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-pic-5.jpg');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/bim-content-bg-5.jpg');">
								<div class="services-toggle-item-head__caption">Удаленное администрирование информационных систем</div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<p>В Удаленное администрирование входит консультирование ИТ-отдела Предприятия.</p>
								<p>Данная услуга является опциональной, предоставляется в случае необходимости и включает в себя инсталляцию и настройку следующих систем:</p>
								<ul>
									<li>Организация раздельного доступа к сети Интернет, мониторинг посещения Интернет-ресурсов сотрудниками компании; формирование статистики посещений
									за любое интересующее Вас время; запрет доступа ко всем нежелательным для Вас ресурсов, как на время (блокирование социальных сетей в рабочее время),
									так и на постоянной основе. Прозрачная для Вас защита от вирусов и сомнительных   	   сайтов;
									<li>Создание и регистрация доменного имени для Вашей организации. Что позволит Вам  	   в дальнейшем создать собственную почту и сайт компании, работа которых не будет зависеть от сторонних организаций. Как результат - сократит расходы и повысит   надежность критических узлов системы;
									<li>Для предотвращения ситуаций, хищения и порчи важной информации существует возможность создания, настройки и последующего администрирования контроллера   	   домена. Контроллер домена – централизованный инструмент управления правами 		   пользователей, позволяющий жестко задать ограничения на любые действия 		   пользователя, в зависимости от ситуации.
									<li>Бесплатные сервисы доставки и отправки электронной почты имеют ряд  ограничений: спам, вирусы, не все письма от таких сервисов доходят до крупных 		   организаций. Мы можем создать и администрировать почтовый сервис, с защитой 		   от спама и вирусов. Письма с такого сервиса уже не будут попадать в спам – почту
									у Ваших клиентов.
									<li>С ростом числа сотрудников, а также объемов информации встает вопрос создания надежного, централизованного места хранения. Мы можем создать 		   отказоустойчивую, журналируемую систему. Распределить права доступа
									для каждого сотрудника, группы или отдела. В такой системе, даже если сотрудник   или коллега по отделу удалит свои файлы – мы всегда сможем их восстановить,
									а также найти виновного.
									<li>Рост объема обрабатываемой информации неизбежен, как и неизбежно появление   	   	   дополнительных серверов: почтовый сервер, файловый сервер, сервер резервного 		   копирования и другие. Мы предлагаем не тратить деньги на покупку дорогостоящего 	   оборудования под каждую из задач, а воспользоваться технологией виртуализации. С помощью данной технологии на одном мощном и надежном сервере запускается 		   несколько, изолированных друг от друга виртуальных машин (по возможностям 		   полностью соответствующие «настоящим» серверам и нисколько
									им не уступающие).  Таким образом вместо покупки несколько дорогих серверов,  мощности которых в конечном счете не используются в полной мере,
									вы оптимизируете свои расходы, так как будете использовать меньшее количество  машин, но задействовав их ресурсы более разумно.
								</ul>
								<p>Воспользовавшись нашими услугами, Вы сможете организовать защищенное рабочее место
									для любого сотрудника вне зависимости от его географического положения. Для нас не составит труда соединить несколько офисов, расположенных в различных зданиях, городах, странах
									в защищенную от взлома сеть. </p>
								<p>Мы всегда подходим с большой ответственностью к вопросам безопасности, поэтому наши клиенты получают максимально возможную защиту от взлома как внешнего (Интернет), так и внутреннего (недобросовестный сотрудник). Благодаря использованию системы оповещений, мы максимально быстро реагируем на все попытки как-то проникнуть в систему и блокируем взломщика. Ежедневно, автоматически система формирует отчет о вирусной активности в сетях наших клиентов,
									что позволяет предупредить и предотвратить вирусную эпидемию.</p>
								<p>Для повышения надежности, мы можем развернуть систему резервного копирования любых данных. Восстановление данных из такой системы занимает минимальное время и позволяет вернуться на достаточно далекую дату (на неделю, месяц, полгода, год).</p>
								<p>Также мы можем настроить и администрировать: </p>
								<ul>
									<li>Систему IP – телефонии;</li>
									<li>Систему видеонаблюдения;</li>
									<li>Сервер 1С: Предприятия;</li>
								</ul>
								<p>Проводя Удалённое администрирование информационных систем мы ведём автоматизированный мониторинг, что позволяет реагировать на возникающие проблемы до того, как они начинают доставлять Вам неудобства.</p>
							</div>
						</div>
						
					</div>
					
				</div>
				-->

		<?$APPLICATION->IncludeComponent("bitrix:news.list", "kartsup_news_list_slider5", Array(
	"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "CODE",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
			3 => "PREVIEW_PICTURE",
			4 => "DETAIL_TEXT",
			5 => "DETAIL_PICTURE",
			6 => "DATE_ACTIVE_FROM",
			7 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "3",	// Код информационного блока
		"IBLOCK_TYPE" => "index_page",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "10000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1"
	),
	false
);?>

				<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_software_square_linear1", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_software_square_linear1",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "3",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>

				<!--
				<section class="products-slider-section">
					<h2>Программные продукты</h2>
					<div class="products-slider-box">
						<div id="productsSlider" class="products-slider swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Копирование параметров</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-3@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Менеджер листов</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT UI reset</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Суперфильтр</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
							</div>
						</div>
						<div class="products-slider-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</section>
				-->
				<section class="projects-section">
					<div class="container">
						<div class="container-1140">
							<h2>Реализованные проекты</h2>
							<div class="projects-box">



								<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider3", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider3"
	),
	false
);?>



								<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsu_projects_complete_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsu_projects_complete_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "1000",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"IBLOCK_ID" => "7",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>



								<!--
								<div class="projects-list row">
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-1.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Русская Промышленная Компания</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-2.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">ООО "Салигрэм"</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-3.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Архитектурная студия МIV</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-4.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Ventsoft</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
								</div>
								-->
							</div>

						</div>

					</div>
				</section>
				
			</div>
			
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
