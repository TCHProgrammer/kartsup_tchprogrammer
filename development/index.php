<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Разработка ПО");
?>
<div class="regular-page-box">
		<div class="container">
			<h1>Разработка ПО</h1>
		</div>
			
		<div class="development-wrapper">
			<div class="container">
				<div class="development-about">
					<h2>Основное направление нашей компании — разработка программного обеспечения</h2>
					<p>Фазе разработке предшествовала фаза анализа, в ходе которой была разработана рекомендуемая архитектура приложения, позволяющая реализовать всю функциональность с учетом требуемых показателей производительности системы.</p>
					<p>Процесс разработки ПО организован следующим образом: после получения требований к системе, мы в короткое время создаем прототип системы, демонстрируем его заказчику, дорабатываем по замечаниям.</p>
				</div>
			</div>
			
			<div class="development-services">
				<div class="container">
					<div class="development-services__box">
						<h2>Наши услуги</h2>
						<ul class="development-services__list blue-marker">
							<li>Разработка ПО, программ специального назначения создание проектов системной интеграции </li>
							<li>Поддержка и развитие существующих программных продуктов</li>
							<li>Аутсорсинг разработки программ (оффшорное программирование)</li>
							<li>Создание профессиональной технической и эксплуатационной документации к программным продуктам и системам</li>
							<li>Полномасштабное тестирование программного обеспечения</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="container">
				<section class="projects-section">
					<div class="container">
						<div class="container-1140">
							<h2>Реализованные проекты</h2>
							<div class="projects-box">


								<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider3", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider3"
	),
	false
);?>


								<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsu_projects_complete_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsu_projects_complete_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "1000",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"IBLOCK_ID" => "7",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>
								<!--
								<div class="projects-list row">
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-1.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Русская Промышленная Компания</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-2.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">ООО "Салигрэм"</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-3.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Архитектурная студия МIV</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12 project-col">
										<a class="project-item" href="#">
											<div class="project-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-4.png" alt="">
											</div>
											<div class="project-item__back">
												<div class="project-item__name">Ventsoft</div>
												<div class="project-item__more more-btn"></div>
											</div>
										</a>
									</div>
								</div>
								-->
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>