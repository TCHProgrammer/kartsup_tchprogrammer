<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Программные продукты. REVIT Менеджер листов");
?>
	
	<div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-3-wrapper">
				<h2>REVIT Менеджер листов</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>Данное дополнение позволяет создавать независимые в плане нумерации альбомы и контролировать их взаимодействие.</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__caption">Что нового:</div>
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-12 col-sm-12 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-3@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Стандартный установщик Windows с возможностью удаления.</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<?
								downloads::displayDownloadReferences('/downloads/software/list-manager','DESC');
							?>

							<!--
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2017</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2016</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2015</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2014</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2013</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2012</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2011</span><s></s></a>
							-->
						</div>
					</aside>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2017</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2016</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2015</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2014</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2013</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2012</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Менеджер листов 2011</span><s></s></a>
				</div>
				
				<div class="product-how-work">
					<div class="product-how-work__caption">Дополнение "Менеджер листов" (high quality)</div>
					<div class="product-how-work__video">
						<iframe src="https://www.youtube.com/embed/Acn7bR1WgO8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				</div>
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>