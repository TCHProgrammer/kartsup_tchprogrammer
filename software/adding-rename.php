<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Программные продукты. REVIT Дополнение Переименовка");
?><div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-5-wrapper">
				<h2>REVIT Дополнение Переименовка</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>Данное дополнение к Revit позволяет составить строковое значение из параметров элемента и записать его в другой строковый параметр или наименование типа семейства.</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__caption">Что нового:</div>
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-12 col-sm-12 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Стандартный установщик Windows с возможностью удаления.</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<a class="arrow-btn download-btn" href="#"><span>REVIT Rename 2014 64x</span><s></s></a>
						</div>
					</aside>
				</div>
				
				<div class="product-info-box">
					<div class="product-info-box__content b-content">
						<p>Данное дополнение к Revit позволяет составить строковое значение из параметров элемента и записать его в другой строковый параметр или наименование типа семейства.</p>
					</div>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>REVIT Rename 2014 64x</span><s></s></a>
				</div>
				
				<div class="product-how-work">
					<div class="product-how-work__caption">Инструмент "Переименовка"</div>
					<div class="product-how-work__video">
						<iframe src="https://www.youtube.com/embed/Uvnvod35ecM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				</div>
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>