<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("REVIT Копирование параметров");
?>


	
	<div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-1-wrapper">
				<h2>REVIT Копирование параметров</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>Стандартный инструмент «Сопоставление свойств типа» не всегда работает так, как хотелось бы. В большинстве случаев он передает данные Типа семейства и лишь некоторые параметры. Данное дополнение позволяет избавиться от этих трудностей.</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__caption">Что нового:</div>
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Поправлены некоторые некорректные отображения параметров.</div>
										</div>
									</div>
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-1-icon-2.png" alt=""></div>
											<div class="product-what-new-item__desc">Присутствует англоязычная версия (Язык дополнения зависит от языковой версии Revit)</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2017</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2016</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2015</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2012</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2011</span><s></s></a>
						</div>
					</aside>
				</div>
				
				<div class="product-info-box">
					<div class="product-info-box__content b-content">
						<ul>
							<li>А что если тип семейства требуется оставить какой был и передать большую часть параметров вхождения?</li>
							<li>Хотите ли вы управлять тем, какие данные передавать?</li>
							<li>Хотите ли вы передавать данные между разными категориями?</li>
							<li>В этом как раз поможет дополнение «Копирование параметров».</li>
							<li>Дополнение "Копирование параметров"</li>
						</ul>
					</div>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2017</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2016</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2015</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2012</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2011</span><s></s></a>
				</div>
				
				<div class="product-how-work">
					<div class="product-how-work__caption">Как работать с данным дополнением:</div>
					<div class="product-how-work-steps">
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">1</div>
							<div class="product-how-work-step__desc">Найдите во вкладке «Надстройки» кнопку «Копировать параметры»</div>
						</div>
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">2</div>
							<div class="product-how-work-step__desc">После нажатия на эту кнопку укажите в рабочей области объект, чьи параметры требуется скопировать</div>
						</div>
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">3</div>
							<div class="product-how-work-step__desc">В диалоговом окне поставьте галочки возле тех параметров, которые вы хотели бы скопировать</div>
						</div>
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">4</div>
							<div class="product-how-work-step__desc">Подтвердите ваш выбор нажатием на кнопку «Копировать параметры»</div>
						</div>
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">5</div>
							<div class="product-how-work-step__desc">Поочередно выбираете объекты, в которые требуется передать параметры</div>
						</div>
						<div class="product-how-work-step">
							<div class="product-how-work-step__num">6</div>
							<div class="product-how-work-step__desc">Нажатием клавиши ESC завершите процесс выбора</div>
						</div>
					</div>
				</div>
				
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>