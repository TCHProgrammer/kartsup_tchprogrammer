<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Программные продукты. REVIT UI reset");
?><div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-6-wrapper">
				<h2>REVIT UI reset</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>Приложение позволяет сбросить настройки интерфейса Revit в случае сбоя.</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-12 col-sm-12 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Работает с Revit 2014 и 2015.</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<a class="arrow-btn download-btn" href="#"><span>Revit UI reset</span><s></s></a>
						</div>
					</aside>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>Revit UI reset</span><s></s></a>
				</div>
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>