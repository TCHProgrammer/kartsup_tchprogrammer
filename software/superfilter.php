<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Программные продукты. REVIT Суперфильтр");
?>
<div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-7-wrapper">
				<h2>REVIT Суперфильтр</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>Данное дополнение позволяет расширить функционал стандартного фильтра Revit с возможностью фильтрации не только по категории но и по типу объекта.</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__caption">Что нового:</div>
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png" srcset="pic/product-icon-7.png 1x, pic/product-icon-7@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Теперь Супер фильтр работает со всеми категориями объектов, в том числе и с объектами выделенными в диспетчере проекта.</div>
										</div>
									</div>
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-7-icon-2.png" alt=""></div>
											<div class="product-what-new-item__desc">Теперь окно представлено в виде постоянно открытого окна по аналогии с окном свойств и диспетчера проекта. Можно его размещать по аналогии с этими окнами, даже делать дополнительной вкладкой.</div>
										</div>
									</div>
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-7-icon-3.png" alt=""></div>
											<div class="product-what-new-item__desc">Обновленный интерфейс как визуально так и по функционалу. Теперь показывается количество объектов в каждом узле. Так же доступен список ID элементов.</div>
										</div>
									</div>
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-7-icon-4.png" alt=""></div>
											<div class="product-what-new-item__desc">Исправлены некоторые недочеты: панель теперь запомитает свое предыдущее состояние и не появляется при открытии нового документа (нужно закрывать панель с помощью кнопки на ленте в табе KartsUp) исправлены некоторые конфликты с другими дополнениями.</div>
										</div>
									</div>
									<div class="product-what-new-item-col col-md-6 col-sm-6 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-7-icon-5.png" alt=""></div>
											<div class="product-what-new-item__desc">В стандартном окне настроек Revit теперь есть раздел суперфильтра для отключения режима панели (дополнение будет работать через стандартное диалоговое окно).</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2017</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2016</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2015</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2014</span><s></s></a>
						</div>
					</aside>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2017</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2016</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2015</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT SuperFilter 2014</span><s></s></a>
				</div>
				
				<div class="product-how-work">
					<div class="product-how-work__caption">Как работать с данным дополнением:</div>
					<p>Интерфейс программы представляет собой панель, в которой выделенные элементы отображаются
					в выпадающих списках. Каждый из списков можно пометить с помощью галочки, тем самым подтвердив
						или отказавшись от выбора элементов этого списка.</p>
					<p>Команда <strong>«Обновить»</strong> полностью переопределяет отмеченные в панели категории в соответствии с текущим выделением элементов в рабочей области Revit.</p>
					<p>Команда <strong>«Добавить»</strong> аналогична команде «Обновить», но список ранее отмеченных категорий не очищается, а дополняется в соответствии с текущим выделениеv элементов в рабочей области Revit.</p>
					<p>Команда <strong>«Выбрать»</strong> выполняет обратную операцию и переопределяет выделение элементов в рабочей области Revit в соответствии со списком категорий, отмеченных галочками в панели SuperFilter.</p>
					<p>Если панель SuperFilter закрыта, восстановить ее можно с помощью команды <strong>SuperFilter</strong> на вкладке <strong>KartsUP</strong> в ленте Revit.</p>
					<br>
					<br>
					<p><img src="pic/product-superfilter-pic.png" alt=""></p>
				</div>
				
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>