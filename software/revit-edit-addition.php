<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("REVIT дополнение редактирования");
?><div class="regular-page-box has-pattern-bg">
		<div class="container">
			<div class="product-wrapper product-2-wrapper">
				<h2>REVIT дополнение редактирования</h2>
				<div class="product-cols clearfix">
					<div class="product-main">
						<div class="product-desc">
							<p>
								В данный набор входят 4 дополнения: Антиотзеркаливание, Суперфильтр, Переименовка и Довести.<br>
								В набор под 2011 Revit дополнение «Довести» пока не входит.<br>
								Идет процесс перевода под 2011 Revit и, как следствие, улучшение функционала.
							</p>
						</div>
						<div class="product-what-new">
							<div class="product-what-new__caption">Что нового:</div>
							<div class="product-what-new__box">
								<div class="product-what-new-items row">
									<div class="product-what-new-item-col col-md-12 col-sm-12 col-xs-12" data-mh="product-what-new-item-col">
										<div class="product-what-new-item">
											<div class="product-what-new-item__icon"><img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt=""></div>
											<div class="product-what-new-item__desc">Обновилась версия набора редакторов от меня. Теперь он идет с новым инсталлером. Автоматический контроль отзеркаленных дверей можно отменить и свободно пользоваться другими дополнениями. Отключается контроль для приложения, незавилимо от открытых документов. Все дополнения находятся в новой вкладке KartsUp. Там же и отключение антиотзеркаливания.</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<aside class="product-aside">
						<div class="product-btns">
							<?
								downloads::displayDownloadReferences('/downloads/software/revit-edit-addition','DESC');
							?>
							<!--
							<a class="arrow-btn download-btn" href="#"><span>REVIT Дополнение редактирования 2012</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Дополнение редактирования 2011 x32</span><s></s></a>
							<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2011 х64</span><s></s></a>
							-->
						</div>
					</aside>
				</div>
				
				<div class="product-main-buttons">
					<a class="arrow-btn download-btn" href="#"><span>REVIT Дополнение редактирования 2012</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Дополнение редактирования 2011 x32</span><s></s></a>
					<a class="arrow-btn download-btn" href="#"><span>REVIT Копирование параметров 2011 х64</span><s></s></a>
				</div>
				
				<div class="product-bottom-email">
					<div class="product-bottom-email__label">Все вопросы можно писать на почту</div>
					<a class="arrow-btn" href="mailto:kart@kartsup.ru"><span>kart@kartsup.ru</span><i></i></a>
				</div>
				
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>