<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Программные продукты");
?><div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider1", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "index_page",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1"
	),
	false
);?>
	<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsu_software_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsu_software_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "3",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>
<!--
<section class="main-slider-section">
		<div class="container">
			<div class="main-slider-section__box clearfix">
				<div class="main-products-list">
					<div class="main-products-list__box">
						<h2>Наши программные продукты</h2>
						<a class="more-btn white-btn" href="#"></a>
						<div class="main-slider-bullets"></div>
						<div class="main-products-items">
							<a class="main-product-item item-1 active" href="#" data-slide="1">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Копирование параметров</div>
							</a>
							<a class="main-product-item item-2" href="#" data-slide="2">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Дополнение редактирования</div>
							</a>
							<a class="main-product-item item-3" href="#" data-slide="3">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Менеджер листов</div>
							</a>
							<a class="main-product-item item-4" href="#" data-slide="4">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Рабочая плоскость</div>
							</a>
							<a class="main-product-item item-5" href="#" data-slide="5">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Дополнение Переименовка</div>
							</a>
							<a class="main-product-item item-6" href="#" data-slide="6">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Revit UI reset</div>
							</a>
							<a class="main-product-item item-7" href="#" data-slide="7">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Суперфильтр</div>
							</a>
						</div>
					</div>
				</div>
				<div class="main-product-desc">
					<div class="main-product-desc__box">
						<div class="main-product-desc-slider__wrap">
							<div id="mainProductDescSlider" class="main-product-desc-slider swiper-container">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Копирование параметров</div>
											<div class="main-product-desc-item__about">Стандартный инструмент «Сопоставление свойств типа» не всегда работает так, как хотелось бы. В большинстве случаев он передает данные Типа семейства и лишь некоторые параметры. Данное дополнение позволяет избавиться от этих трудностей.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Дополнение редактирования</div>
											<div class="main-product-desc-item__about">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet est efficitur, auctor odio quis, placerat enim. Donec quis ligula in enim rhoncus gravida</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Менеджер листов</div>
											<div class="main-product-desc-item__about">Nullam ut condimentum massa. Nulla maximus magna nec cursus viverra. Suspendisse scelerisque suscipit iaculis Nullam ut condimentum massa. Nulla maximus magna nec cursus viverra. Suspendisse scelerisque suscipit iaculis</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Рабочая плоскость</div>
											<div class="main-product-desc-item__about">Fusce venenatis mi at nibh tristique, sed rhoncus dui feugiat. Nunc gravida sollicitudin dui a malesuada. Fusce venenatis mi at nibh tristique, sed rhoncus dui feugiat. Nunc gravida sollicitudin dui a malesuada.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Дополнение Переименовка</div>
											<div class="main-product-desc-item__about">Duis tincidunt diam sollicitudin ullamcorper tristique. In purus lacus, viverra eget diam sit amet, ornare varius nisi. Duis tincidunt diam sollicitudin ullamcorper tristique. In purus lacus, viverra eget diam sit amet, ornare varius nisi.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Revit UI reset</div>
											<div class="main-product-desc-item__about">Aliquam aliquet interdum leo eu tempor. Duis faucibus bibendum varius. Pellentesque porttitor libero dui, in auctor felis fermentum id Aliquam aliquet interdum leo eu tempor. Duis faucibus bibendum varius. Pellentesque porttitor libero dui, in auctor felis fermentum id</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Суперфильтр</div>
											<div class="main-product-desc-item__about">Donec volutpat enim nec est convallis tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus ac mauris vel tellus maximus luctus Donec volutpat enim nec est convallis tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus ac mauris vel tellus maximus luctus</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="main-product-desc-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	-->
	
	<section class="bim-info-section">
		<div class="container">
			<div class="bim-info-box">
				<div class="bim-info-content">
					<h2>Удаленный BIM-менеджмент для проектных организаций</h2>
					<p>
						<strong>BIM</strong> (Building Information Model) – это новый подход
						к проектированию, в котором здание рассматривается
						как единый объект, каждый из элементов которого обладает влияющими друг на друга и на объект в целом параметрами
						и свойствами. <strong>BIM</strong> – это не конкретное программное обеспечение, а технология проектирования в которой за основу берётся не плоскостной чертёж, а 3D-модель. Для более эффективного перехода на данную технологию необходимо ввести дополнительного высокооплачиваемого специалиста
						в штат организации – <strong>BIM</strong>-менеджера.
					</p>
				</div>
				<div class="bim-more-btn"><a class="arrow-btn" href="#"><span>Читать дальше</span><i></i></a></div>
			</div>
		</div>
	</section>


<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider2", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider2"
	),
	false
);?>




	<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_software_square_linear", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_software_square_linear",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "4",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>


	<!--
	<section class="main-services-section">
		<div class="container">
			<div class="main-services-list">
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-1.jpg');"></div>
					<div class="main-service-item__caption">Разработка Стандарта Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-2.jpg');"></div>
					<div class="main-service-item__caption">Обучение сотрудников Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-3.jpg');"></div>
					<div class="main-service-item__caption">Организация работы над проектом в программном обеспечении (ПО)</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-4.jpg');"></div>
					<div class="main-service-item__caption">Консультации <nobr>ИТ-отдела</nobr> предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-5.jpg');"></div>
					<div class="main-service-item__caption">Удаленное администрирование информационных систем</div>
					<span class="more-btn"></span>
				</a>
			</div>
		</div>
	</section>
	-->
	
	<section class="main-about-section">
		<div class="container">
			<div class="main-about-section__box">
				<div class="main-about-content">
					<h2>О компании</h2>
					<p>
						Наша компания занимается разработкой програмного обеспечения. Мы не ограничиваем себя рамками одной отрасли и стараемся изучать все новое. На данном этапе
						мы плотно работаем над такими направлениями как базы данных, 3D представление и навигация, разработки
						для помощи в проектировании (дополнительные модули
						для программного продукта Revit). Всё это помогает
						нам вывести наши услуги по <strong>BIM</strong>-менеджменту на более
						высокий технологический уровень.
					</p>
				</div>
				<div class="main-about-more-btn"><a class="arrow-btn white-btn" href="#"><span>Читать дальше</span><i></i></a></div>
			</div>
		</div>
	</section>


	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider9", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "DETAIL_PICTURE",
			4 => "DATE_ACTIVE_FROM",
			5 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider9"
	),
	false
);?>

	<!--
	<section class="main-news-section">
		<div class="container">
			<div class="main-news-box">
				<h2>Новости</h2>
				<div class="main-news-list">
					<a class="main-news-item news-item-1 main-news" href="#">
						<div class="main-news-item__bg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-news-pic-1.jpg');"></div>
						<div class="main-news-item__content">
							<div class="main-news-item__caption"><span>Обновления REVIT</span></div>
							<div class="main-news-item__date">30 июня 2017</div>
						</div>
					</a>
					<a class="main-news-item news-item-2" href="#">
						<div class="main-news-item__bg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-news-pic-2.jpg');"></div>
						<div class="main-news-item__content">
							<div class="main-news-item__caption"><span>Менеджер листов 2016</span></div>
							<div class="main-news-item__date">30 июня 2017</div>
						</div>
					</a>
					<a class="main-news-item news-item-3" href="#">
						<div class="main-news-item__bg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-news-pic-3.jpg');"></div>
						<div class="main-news-item__content">
							<div class="main-news-item__caption"><span>SuperFilter для Revit 2015</span></div>
							<div class="main-news-item__date">12 августа 2014</div>
						</div>
					</a>
					<a class="main-news-item news-item-4" href="#">
						<div class="main-news-item__bg" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-news-pic-4.jpg');"></div>
						<div class="main-news-item__content">
							<div class="main-news-item__caption"><span>Менеджер листов 2016</span></div>
							<div class="main-news-item__date">30 июня 2017</div>
						</div>
					</a>
				</div>
				<div class="main-news-more"><a class="arrow-btn" href="#"><span>Смотреть все новости</span><i></i></a></div>
			</div>
		</div>
	</section>
	-->
	
	<section class="projects-section">
		<div class="container">
			<div class="container-1140">
				<h2>Реализованные проекты</h2>
				<div class="projects-box">

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_slider3", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider3"
	),
	false
);?>

					<?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsu_projects_complete_slider", 
	array(
		"COMPONENT_TEMPLATE" => "kartsu_projects_complete_slider",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "1000",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "BIM_MANAGEMENT",
		"IBLOCK_ID" => "7",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?>

					<!--


					<div class="projects-list row">
						<div class="col-md-3 col-sm-6 col-xs-12 project-col">
							<a class="project-item" href="#">
								<div class="project-item__logo">
									<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-1.png" alt="">
								</div>
								<div class="project-item__back">
									<div class="project-item__name">Русская Промышленная Компания</div>
									<div class="project-item__more more-btn"></div>
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 project-col">
							<a class="project-item" href="#">
								<div class="project-item__logo">
									<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-2.png" alt="">
								</div>
								<div class="project-item__back">
									<div class="project-item__name">ООО "Салигрэм"</div>
									<div class="project-item__more more-btn"></div>
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 project-col">
							<a class="project-item" href="#">
								<div class="project-item__logo">
									<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-3.png" alt="">
								</div>
								<div class="project-item__back">
									<div class="project-item__name">Архитектурная студия МIV</div>
									<div class="project-item__more more-btn"></div>
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 project-col">
							<a class="project-item" href="#">
								<div class="project-item__logo">
									<img src="<?=SITE_TEMPLATE_PATH?>/pic/project-logo-4.png" alt="">
								</div>
								<div class="project-item__back">
									<div class="project-item__name">Ventsoft</div>
									<div class="project-item__more more-btn"></div>
								</div>
							</a>
						</div>
					</div>
					-->
				</div>
			</div>
		</div>
	</section>
	


<div>

</div>
 <br>
</div>
<div>
 <br>
</div>
<div>

</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>