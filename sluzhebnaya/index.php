<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("KartsUP. Служебная");
?>

<div class="page-wrapper">
	
	<div class="container">
		<div class="b-content">
			<br>
			<h1>Заголовок H1</h1>
			<p>Integer pellentesque lectus vel commodo viverra. Pellentesque bibendum ac tortor eget accumsan. Curabitur eu eros id neque sagittis interdum. Nullam iaculis, odio eu sollicitudin ornare, velit metus aliquet tellus, in commodo tortor dui rhoncus velit. Duis facilisis hendrerit metus, ac tincidunt erat porttitor ultricies. Quisque non molestie massa. Sed ut libero tellus. Aenean quam justo, viverra eu porttitor at, maximus nec massa. Maecenas luctus feugiat magna, vitae vestibulum neque lacinia at. Mauris porttitor tellus sed enim tincidunt, non tempor ipsum scelerisque. Mauris erat mauris, rhoncus a mauris ac, efficitur porttitor leo. Vivamus lobortis ullamcorper diam vitae finibus.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eveniet iste non odit quia soluta ullam veniam? Aliquam deserunt ducimus eos, illo incidunt iste maiores omnis quisquam repudiandae totam velit.</p>
			<p>Aliquam ac ornare nisi. Quisque mollis id est et fermentum. Sed eu tempus erat, vitae pellentesque nibh. Ut eget mi dolor. Proin et metus elementum, dapibus purus vel, ultrices metus. Integer at lacus eget risus facilisis suscipit. Donec faucibus magna nisi, sit amet facilisis ante aliquet id. Donec vel arcu ac ligula fermentum semper nec vitae metus. Aenean congue enim ac sapien sodales, quis mattis turpis aliquet.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aut, blanditiis commodi consectetur corporis cumque dolorum, eligendi eum explicabo illum minima minus nemo possimus suscipit, vitae! Aperiam earum eius libero?</p>
			<br>
			<h2>Заголовок H2</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aperiam eaque eligendi, esse, et iste laboriosam non officiis perspiciatis placeat quae quam quibusdam quo recusandae repellat repudiandae sint tenetur voluptas.</p>
			<br>
			<h3>Заголовок H3</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam cumque delectus dignissimos eaque officiis temporibus tenetur ut voluptate. A dolore eligendi facere itaque laborum, natus necessitatibus obcaecati officiis suscipit totam.</p>
			<ol>
				<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae purus eget dui congue ultricies. Aenean consequat urna vel orci imperdiet ultricies. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet atque, consectetur dignissimos doloremque ducimus eos facilis, harum id incidunt magnam neque nisi nobis obcaecati officia quidem quisquam quos reiciendis vitae!</li>
				<li>Aliquam luctus gravida justo in vestibulum. Donec ante ipsum, semper ut tincidunt et, vestibulum ullamcorper nunc.</li>
				<li>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquid aperiam asperiores consequuntur cupiditate dicta distinctio excepturi incidunt, inventore ipsum iste mollitia possimus quam qui quibusdam quis soluta ut, voluptate.
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur illum incidunt, laborum, mollitia, non nostrum nulla praesentium provident quaerat quia tempore ut. Eos nemo neque nulla numquam quae saepe, voluptatibus?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur illum incidunt, laborum, mollitia, non nostrum nulla praesentium provident quaerat quia tempore ut. Eos nemo neque nulla numquam quae saepe, voluptatibus?</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur illum incidunt, laborum, mollitia, non nostrum nulla praesentium provident quaerat quia tempore ut. Eos nemo neque nulla numquam quae saepe, voluptatibus?</li>
					</ul>
				</li>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum debitis facilis odit omnis perferendis quae, quam similique! Aliquid aspernatur doloremque eos harum hic libero optio quae quasi sunt veniam?</li>
			</ol>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dolore eligendi fuga ipsa ipsum libero quae quisquam? Aperiam delectus dignissimos eius, excepturi, id minus optio quae quis recusandae saepe sit?</p>
			<hr>
			<h3>Lorem ipsum dolor sit amet</h3>
			<ul>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem earum eveniet id illo illum? Cumque eligendi odio quae quisquam ratione sunt veritatis! Aperiam et libero, maiores perferendis provident quisquam.</li>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi eveniet fuga harum quaerat quos. Aliquid architecto distinctio ea earum ipsam laborum molestiae, mollitia non numquam provident quae quas qui, voluptatem!</li>
			</ul>
			<hr>
			<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor dolore doloribus expedita hic inventore iure molestias nisi nulla odit quibusdam quisquam repellendus reprehenderit sunt tempora, ut? Aliquam delectus hic nam.</div>
			<br>
			<h4>Заголовок H4</h4>
			<p><strong>Donec ante ipsum</strong> dolor sit amet, <a href="#">consectetur adipisicing elit</a>. Alias aut beatae corporis dignissimos excepturi harum id, illo, incidunt iure laboriosam laudantium nam nobis perferendis possimus ratione, ut voluptatibus. Aperiam, modi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus deserunt dignissimos, dolore dolorem ea, et explicabo ipsam labore minima, nisi nobis odit omnis quam quasi rerum sapiente totam vel voluptate.</p>
			<h5>Заголовок H5</h5>
			<p><i>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur et exercitationem inventore libero nostrum, officiis quae quasi. Asperiores culpa exercitationem iste labore libero necessitatibus nihil quibusdam suscipit temporibus veniam.</i></p>
			<p>
				<i>Элемент i</i><br>
				<em>Элемент em</em><br>
				<b>Элемент b</b><br>
				<strong>Элемент strong</strong><br>
				<u>Элемент u</u><br>
				<s>Элемент s</s>
			</p>
			<br>
			<h2>Форма</h2>
			<form action="#" name="#">
				<div class="input-box">
					<input type="text" placeholder="Имя">
				</div>
				<div class="input-box has-error">
					<input type="text" placeholder="Имя" value="Невалидное поле">
				</div>
				<div class="input-box">
					<input type="text" placeholder="E-mail">
				</div>
				<div class="input-box">
					<textarea placeholder="Комментарий"></textarea>
				</div>
				<div class="submit-box">
					<button style="width: 250px;" class="arrow-btn" type="submit">Отправить</button>
				</div>
			</form>
			<br>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet architecto culpa cumque dolor eius, ipsa itaque quisquam reprehenderit similique vitae, voluptatem. Aperiam atque commodi, magnam quas quidem rerum vero!</p>
			<ul>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias at aut dolor dolorum, ea eos exercitationem, explicabo facilis fugit id maxime mollitia, nostrum quisquam ratione tempora temporibus ut voluptas voluptatum!</li>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi consectetur dolorum, eaque eum excepturi harum, hic ipsa placeat sunt temporibus, totam velit veniam. Beatae eligendi nam nemo nisi nostrum, placeat.</li>
			</ul>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam deserunt enim officiis quidem reiciendis soluta temporibus? Aspernatur culpa cupiditate debitis libero rem. Accusantium blanditiis distinctio fugit illum in, quis quisquam!</p>
			<ol>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias at aut dolor dolorum, ea eos exercitationem, explicabo facilis fugit id maxime mollitia, nostrum quisquam ratione tempora temporibus ut voluptas voluptatum!</li>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi consectetur dolorum, eaque eum excepturi harum, hic ipsa placeat sunt temporibus, totam velit veniam. Beatae eligendi nam nemo nisi nostrum, placeat.</li>
			</ol>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda corporis cumque, delectus dignissimos, dolores dolorum ea eius impedit in itaque iusto, magni molestiae omnis possimus quaerat temporibus voluptate. Similique!</p>
			
			<br>
		</div>
	</div>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>