<? define('STOP_STATISTICS', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$GLOBALS['APPLICATION']->RestartBuffer();

CModule::IncludeModule("iblock");

//define("LANGUAGE_ID", 'RU');



function convertDate($date)
{
   $components = explode (" ", $date, 2);
   $monthes    = array
   (
      'января', 
      'февраля', 
      'марта', 
      'апреля', 
      'мая', 
      'июня', 
      'июля', 
      'августа', 
      'сентября', 
      'октября', 
      'ноября', 
      'декабря'
   );
   $date = explode ('.', $components[0], 3);
   
   return (trim($date[0], '0')." ".$monthes[((int)($date[1])-1)]." ".$date[2]);
}


$PAGE_SIZE = $_POST['page_size'];

//var_dump(CLanguage::GetList());

$arSelect = Array("IBLOCK_ID","ID");
$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$to_count = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
//$to_count->NavStart(1);
$total_elements_count = intval($to_count->SelectedRowsCount());

$from_start_elements_count = ($_GET['PAGE_NUMBER'] - 1) * $PAGE_SIZE;


$rest_elements_count = $total_elements_count - $from_start_elements_count;






$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array('DATE_ACTIVE_FROM'=>'DESC', 'SORT'=>'ASC'), $arFilter, false, Array("nPageSize"=>$PAGE_SIZE,"iNumPage"=>$_GET['PAGE_NUMBER']), $arSelect);

$count = 0;

while($ob = $res->GetNextElement())
{

	$count++;

	if($rest_elements_count >= $count){
 $arItem = $ob->GetFields();
	//print_r($arFields);

?>
<a class="news-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>"  data-ajax-element-id="<?echo $arItem["ID"]?>">
								<div class="news-item__date"><?echo convertDate($arItem["DATE_ACTIVE_FROM"]);?></div>
								<div class="news-item__caption"><?echo $arItem["NAME"]?></div>
								<div class="news-item__preview"><?echo $arItem["PREVIEW_TEXT"];?></div>
							</a>

<?} if($rest_elements_count <= $PAGE_SIZE):?>
	<div class="stop_ajax_pager_indicator" style="display:none;">
	</div>
<?endif;?>
<?}?>

<?



/*
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list_ajax", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "1",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_ajax"
	),
	false
);
*/?>

