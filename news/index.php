<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости компании");
?><div class="regular-page-box has-pattern-bg news-pattern-bg">
	<div class="container">
		<h1>Новости</h1>
		 <?$APPLICATION->IncludeComponent("bitrix:news", "kartsup_news_total", Array(
	"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "Y",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DETAIL_DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DETAIL_FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "CODE",
			2 => "NAME",
			3 => "PREVIEW_TEXT",
			4 => "PREVIEW_PICTURE",
			5 => "DETAIL_TEXT",
			6 => "DETAIL_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "IBLOCK_ID",
			9 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"DETAIL_PAGER_TEMPLATE" => "",	// Название шаблона
		"DETAIL_PAGER_TITLE" => "Страница",	// Название категорий
		"DETAIL_PROPERTY_CODE" => array(	// Свойства
			0 => "FILES_TO_DOWNLOAD",
			1 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",	// Устанавливать канонический URL
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "1",	// Инфоблок
		"IBLOCK_TYPE" => "news",	// Тип инфоблока
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"LIST_FIELD_CODE" => array(	// Поля
			0 => "ID",
			1 => "CODE",
			2 => "NAME",
			3 => "PREVIEW_TEXT",
			4 => "PREVIEW_PICTURE",
			5 => "DATE_ACTIVE_FROM",
			6 => "",
		),
		"LIST_PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
		"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
		"NEWS_COUNT" => "2",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"SEF_FOLDER" => "/news/",	// Каталог ЧПУ (относительно корня сайта)
		"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
		"SEF_URL_TEMPLATES" => array(
			"detail" => "#ELEMENT_ID#/",
			"news" => "",
			"section" => "",
		),
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела
		"USE_CATEGORIES" => "N",	// Выводить материалы по теме
		"USE_FILTER" => "N",	// Показывать фильтр
		"USE_PERMISSIONS" => "N",	// Использовать дополнительное ограничение доступа
		"USE_RATING" => "N",	// Разрешить голосование
		"USE_RSS" => "N",	// Разрешить RSS
		"USE_SEARCH" => "N",	// Разрешить поиск
		"USE_SHARE" => "N",	// Отображать панель соц. закладок
	),
	false
);?>

		<script type="text/javascript">
			$('.news-list-more-btn a').attr('onclick','');
		</script>

<br>
		 <?/*$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"kartsup_news_list", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "2",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "kartsup_news_list"
	),
	false
);*/?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"kartsup_news_list_slider5",
	Array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "kartsup_news_list_slider1",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"CODE",1=>"NAME",2=>"PREVIEW_TEXT",3=>"PREVIEW_PICTURE",4=>"DETAIL_TEXT",5=>"DETAIL_PICTURE",6=>"DATE_ACTIVE_FROM",7=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "index_page",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> <?/*$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.list", 
	"kartsup_software_square_linear1", 
	array(
		"COMPONENT_TEMPLATE" => "kartsup_software_square_linear1",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "index_page",
		"IBLOCK_ID" => "3",
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "N"
	),
	false
);*/?> <!--
		<section class="products-slider-section">
					<h2>Программные продукты</h2>
					<div class="products-slider-box">
						<div id="productsSlider" class="products-slider swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Копирование параметров</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-3.png" srcset="pic/product-icon-3.png 1x, pic/product-icon-3@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Менеджер листов</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT UI reset</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Суперфильтр</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
							</div>
						</div>
						<div class="products-slider-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</section>

			--> <!--
			<div class="news-wrapper">
				
				<div class="news-list-box">
					<div class="news-list">
						<a class="news-item" href="#">
							<div class="news-item__date">30 июня 2017</div>
							<div class="news-item__caption">Обновления REVIT 2018</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date"> 12 ноября 2015</div>
							<div class="news-item__caption">Обновление дополнений «антиотзеркаливание» (версия 1.6)</div>
							<div class="news-item__preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi consequatur culpa dolores, earum iusto minus neque nihil nostrum odio odit officia, totam vel! Corporis excepturi maiores nihil. Est, in.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
						<a class="news-item" href="#">
							<div class="news-item__date">24 мая 2015</div>
							<div class="news-item__caption">Обновления REVIT 2015</div>
							<div class="news-item__preview">Lorem ipsum – псевдо-латинский текст, который используется для веб дизайна, типографии, оборудования, и распечатки вместо английского текста.</div>
						</a>
					</div>
					<div class="news-list-more-btn"><a class="arrow-btn" href="#">Больше новостей</a></div>
				</div>
				
				<section class="products-slider-section">
					<h2>Программные продукты</h2>
					<div class="products-slider-box">
						<div id="productsSlider" class="products-slider swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-1.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-1@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Копирование параметров</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-3.png" srcset="pic/product-icon-3.png 1x, pic/product-icon-3@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Менеджер листов</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT UI reset</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
								<div class="swiper-slide">
									<a class="products-slide-item" href="#">
										<div class="products-slide-item__head">
											<div class="products-slide-item__icon">
												<img src="pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">
											</div>
										</div>
										<div class="products-slide-item__body">
											<div class="products-slide-item__caption">REVIT Суперфильтр</div>
										</div>
										<span class="more-btn"></span>
									</a>
								</div>
							</div>
						</div>
						<div class="products-slider-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</section>
			
			</div>
			-->
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>