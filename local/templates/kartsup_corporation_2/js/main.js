// remap jQuery to $
(function ($) {

	/* trigger when page is ready */
	$(document).ready(function () {
		
		var $body = $("body");
		var $html = $("html");
		
		// phone mask
		$(".phone-mask").inputmask("+7(999)999-99-99");
		
		// fancybox
		$("[data-fancybox]").fancybox({
			animationEffect: "zoom-in-out",
			touch: false,
			autoFocus: false,
			i18n: {
				en: {
					CLOSE: "Закрыть"
				}
			}
		});
		
		// init wow animations
		if(!$html.hasClass("mobile")){
			new WOW().init();
		}
		
		// smooth scroll to top
		var $scrollToTopBtn = $("#scrollToTop");
		$scrollToTopBtn.on("click", function (e) {
			e.preventDefault();
			$("html, body").animate({
				scrollTop: 0
			}, 300);
			return false;
		});
		function showScrollToTopBtn(){
			var bodyTopOffset = $(document).scrollTop();
			if (bodyTopOffset > 400) {
				$scrollToTopBtn.addClass("visible");
			} else {
				$scrollToTopBtn.removeClass("visible");
			}
		}
		showScrollToTopBtn();
		$(window).scroll(function () {
			showScrollToTopBtn();
		});
		
		
		// responsive nav
		var $openRespNav = $(".open-resp-nav");
		var $RespNavBox = $(".responsive-nav-wrap");
		$openRespNav.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("is-active");
			$RespNavBox.toggleClass("opened");
			$body.toggleClass("added-overlay");
		});
		$(document).on("click", function (e) {
			if (($(e.target).closest($(".responsive-nav-wrap")).length) || ($(e.target).closest($(".open-resp-nav")).length)) return;
			$openRespNav.removeClass("is-active");
			$RespNavBox.removeClass("opened");
			$body.removeClass("added-overlay");
			e.stopPropagation();
		});
		
		// main products slider
		var mainProductDescSlider = new Swiper ('#mainProductDescSlider', {
			loop: true,
			slidesPerView: 1,
			spaceBetween: 5,
			speed: 400,
			autoHeight: true,
			pagination: {
				el: '.main-slider-bullets',
				clickable: true
			},
			navigation: {
				nextEl: '.next-project-slide',
				prevEl: '.prev-project-slide',
			}
		});
		var $mainProductItem = $(".main-product-item");
		$mainProductItem.on("click", function (e) {
			e.preventDefault();
			$('html, body').animate({
				scrollTop: $(".main-product-desc").offset().top
			}, 200);
			var $this = $(this);
			$mainProductItem.removeClass("active");
			$this.addClass("active");
			var slideNumber = $this.data("slide");
			mainProductDescSlider.slideTo(slideNumber);
		});
		var totalSlidesCounter = $mainProductItem.length;
		var $currentSlide = $(".main-product-desc-controls .slide-counter .current");
		var $totalSlides = $(".main-product-desc-controls .slide-counter .total");
		mainProductDescSlider.on('slideChange', function () {
			var currentSlideIndex = mainProductDescSlider.realIndex + 1;
			$mainProductItem.removeClass("active");
			$(".main-product-item.item-" + currentSlideIndex).addClass("active");
			$currentSlide.text(currentSlideIndex);
		});
		$totalSlides.text(totalSlidesCounter);
		
		// top products slider
		var topProductsSlider = new Swiper ('#topProductsSlider', {
			loop: false,
			slidesPerView: 7,
			speed: 400,
			pagination: {
				el: '.prod-slider-bullets',
				clickable: true
			},
			navigation: {
				nextEl: '.next-product-slide',
				prevEl: '.prev-product-slide',
			},
			breakpoints: {
				991: {
					slidesPerView: 5,
				},
				880: {
					slidesPerView: 4
				},
				767: {
					slidesPerView: 3
				},
				570: {
					slidesPerView: 2
				},
				370: {
					slidesPerView: 1
				}
			}
		});
		if($("#topProductsSlider").length){
			topProductsSlider.slideTo($(".products-head-item.current").parent().index());
		}
		
		
		// regular products slider
		var totalProdsCounter = $(".products-slide-item").length;
		var productsSlider = new Swiper ('#productsSlider', {
			loop: true,
			slidesPerView: "auto",
			spaceBetween: 45,
			speed: 400,
			autoHeight: true,
			navigation: {
				nextEl: '.next-project-slide',
				prevEl: '.prev-project-slide',
			},
			breakpoints: {
				767: {
					slidesPerView: 1,
					spaceBetween: 10,
				}
			}
		});
		var $currentProdSlide = $(".products-slider-controls .slide-counter .current");
		var $totalProdsSlides = $(".products-slider-controls .slide-counter .total");
		productsSlider.on('slideChange', function () {
			var currentSlideIndex = productsSlider.realIndex + 1;
			$currentProdSlide.text(currentSlideIndex);
		});
		$totalProdsSlides.text(totalProdsCounter);
		
		
		// toggled services
		var $toggleServiceBtn = $(".toggle-btn");
		$toggleServiceBtn.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("active");
			$this.closest(".services-toggle-item").find(".services-toggle-item-body").slideToggle(300);
			if($this.hasClass("active")){
				$('html, body').animate({
					scrollTop: $this.closest(".services-toggle-item").find(".services-toggle-item-body").offset().top
				}, 300);
			} else {
				$('html, body').animate({
					scrollTop: $this.closest(".services-toggle-item").find(".services-toggle-item-head").offset().top
				}, 300);
			}
		});
		
		
		// edit lk inputs
		var $editThisInput = $(".edit-this-input");
		$editThisInput.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.parent().find("input").select();
		});
		
		
		/* footer */
		
		// products switcher
		var $footerProductsSwitcher = $(".footer-products-switcher");
		$footerProductsSwitcher.on("click", function (e) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass("rotate");
			$this.siblings(".footer-products__list").slideToggle(150);
		});
		
		
	});

})(window.jQuery);
