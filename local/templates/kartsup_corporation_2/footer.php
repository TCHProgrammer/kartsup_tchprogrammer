<footer class="footer">

		<div class="container">

			<div class="footer-box clearfix">

				<div class="footer-right clearfix">

					<div class="footer-right__left">

						<div class="footer-logo">

							<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/pic/footer-logo.png" alt="KartsUP. Разработка программного обеспечения"></a>

						</div>

						<div class="footer-phone"><a href="tel:+79038443648">+7(903)-844-36-48</a></div>

					</div>

					<div class="footer-right__socials">

						<div class="footer-socials-list">

							<a class="fa fa-vk" href="#" title="ВКонтакте" target="_blank"></a>

							<a class="fa fa-facebook" href="#" title="Facebook" target="_blank"></a>

							<a class="fa fa-odnoklassniki" href="#" title="Одноклассники" target="_blank"></a>

							<a class="fa fa-instagram" href="#" title="Instagram" target="_blank"></a>

						</div>

					</div>

				</div>

				<div class="footer-left clearfix">

					<div class="footer-products">

						<div class="footer-products__caption footer-products-switcher">Программные продукты</div>

						<ul class="footer-products__list">

							<li><a href="/software/index.php">Revit Копирование параметров</a></li>
							<li><a href="/software/revit-edit-addition.php">Revit Дополнение редактирования</a></li>
							<li><a href="/software/list-manager.php">Revit Менеджер листов</a></li>
							<li><a href="/software/work-area.php">Revit Рабочая плоскость</a></li>
							<li><a href="/software/adding-rename.php">Revit Дополнение Переименовка</a></li>
							<li><a href="/software/revit-ui-reset.php">Revit UI reset</a></li>
							<li><a href="/software/superfilter.php">Revit Суперфильтр</a></li>

						</ul>

					</div>

					<nav class="footer-nav">

						<ul>

							<li><a href="/software/">Разработка ПО</a></li>

							<li><a href="/bim-management/">BIM-менеджмент</a></li>

							<li><a href="/news/">Новости</a></li>

							<li><a href="/portfolio/">Портфолио</a></li>

							<li><a href="/about/">О компании</a></li>

							<li><a href="/contacts/">Контакты</a></li>

						</ul>

					</nav>

				</div>

				<div class="footer-developer"><span>Сделано в</span><a href="http://www.intensa.ru/" target="_blank"></a></div>

			</div>

		</div>

	</footer>

	

	<a id="scrollToTop" class="scroll-to-top fa fa-angle-up" href="#"></a>



</div>





<!-- modal windows -->



<div id="loginModal" class="modal-window login-modal">

	<div class="modal-window__box">

		<?/*$APPLICATION->IncludeComponent(
                "bitrix:system.auth.form",
                "kartsup_modal",
                Array(
                    "REGISTER_URL" => "",
                    "FORGOT_PASSWORD_URL" => "",
                    "PROFILE_URL" => "/personal/profile/",
                    "SHOW_ERRORS" => "Y"
                )
);*/?>

		<form class="modal-form" action="#" name="login-form">

			<div class="input-box">

				<input type="email" name="email" required placeholder="E-mail">

			</div>

			<div class="input-box">

				<input type="password" name="password" required placeholder="Пароль">

			</div>

			<div class="options-box clearfix">

				<div class="another-pc">

					<label class="checkbox-label">

						<input name="agree" type="checkbox"><i></i>

						<span><i>Чужой компьютер</i></span>

					</label>

				</div>

				<div class="forget-pass"><a href="/auth/password-recovering.php?forgot_password=yes">Забыли пароль?</a></div>

			</div>

			<div class="submit-box">

				<button class="arrow-btn dark-blue-btn">Ok</button>

			</div>

			<div class="privacy-policy-info">Выполняя вход, вы принимаете <a href="#" target="_blank">Политику конфиденциальности</a></div>
			<div class="modalLoginError" style="color:red;"></div>
		</form>
		

	</div>

</div>



<div id="regModal" class="modal-window reg-modal">

	<div class="modal-window__box">

		<div class="modal-window__caption">Регистрация</div>

		<form class="modal-form" action="#" name="reg-form">

			<div class="input-box">

				<input type="text" name="" placeholder="Фамилия, имя">

			</div>

			<div class="input-box">

				<input type="text" name="" placeholder="Организация">

			</div>

			<div class="input-box">

				<input type="text" name="" placeholder="Профессия">

			</div>

			<div class="input-box">

				<input type="email" name="email" required placeholder="E-mail">

			</div>

			<div class="input-box">

				<input type="password" name="password" required placeholder="Пароль">

			</div>

			<div class="input-box">

				<input type="password" name="password-repeat" required placeholder="Повторите пароль">

			</div>

			<div class="submit-box">

				<button class="arrow-btn dark-blue-btn">Ok</button>

			</div>

			<div class="privacy-policy-info">Выполняя вход, вы принимаете <a href="#" target="_blank">Политику конфиденциальности</a></div>
			<div class="modalRegError" style="color:red;"></div>
		</form>

	</div>

</div>

<div id="addCommentModal" class="modal-window add-comment-modal">
	<div class="modal-window__box">
		<form class="contacts-feedback-form row" action="#" name="feedback">
			<div class="col-md-6 col-sm-12 col-xs-12 contacts-feedback-form-col">
				<div class="input-box">
					<input type="text" name="name" placeholder="* Ваше имя" required>
				</div>
				<div class="input-box">
					<input type="email" name="email" placeholder="* E-mail" required>
				</div>
				<div class="input-box">
					<input class="phone-mask" type="tel" name="phone" placeholder="* Номер телефона" required>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 contacts-feedback-form-col">
				<textarea name="comment" placeholder="* Ваше сообщение" required></textarea>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 submit-box">
				<button class="arrow-btn dark-blue-btn" type="submit">Отправить</button>
			</div>
		</form>
	</div>
</div>



<!-- / modal windows -->



<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.fancybox.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/swiper.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.inputmask.bundle.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.matchHeight-min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/wow.min.js"></script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    // ловим событие отправки формы



	//$('#loginModal .submit-box button')
	$('#loginModal form.modal-form').submit(function(e){
		/*$('#loginModal form.modal-form')*/
		//e.preventDefault();
        // хорошим тоном будет сделать минимальную проверку формы перед отправкой
        // хотя бы на заполненность всех обязательных полей
        // в целях краткости здесь она не приводится
 
        var path = '/ajax/auth.php'; // объявляем путь к ajax-скрипту авторизации
        var formData = $('#loginModal form.modal-form').serialize(); // выдергиваем данные из формы
         
        // объявляем функцию, которая принимает данные из скрипта path
        var success = function( response ){
			response = JSON.parse(response);
            if (response['STATUS'] == 'OK')
            {

                // если авторизация успешна, по-варварски перезагрузим страницу
				window.location.href = window.location.href;
            }
            else
            {
                // в противном случае в переменной response будет текст ошибки
                // и его нужно где-то отобразить
                $('.modalLoginError').html( response['MESSAGE'] ).show();
            }           
        };
 
        // явно указываем тип возвращаемых данных
        var responseType = 'html';
 
        // делаем ajax-запрос
        $.post( path, formData, success, responseType );
 
        return false; // не даем форме отправиться обычным способом
    });

	$('#regModal form.modal-form .submit-box button').click(function(e){
		/*$('#loginModal form.modal-form')*/
		//e.preventDefault();
        // хорошим тоном будет сделать минимальную проверку формы перед отправкой
        // хотя бы на заполненность всех обязательных полей
        // в целях краткости здесь она не приводится
 
        var path = '/ajax/register.php'; // объявляем путь к ajax-скрипту авторизации
        var formData = $('#regModal form.modal-form').serialize(); // выдергиваем данные из формы
         
        // объявляем функцию, которая принимает данные из скрипта path
        var success = function( response ){
			response = JSON.parse(response);
            if (response['STATUS'] == 'OK')
            {
                // если авторизация успешна, по-варварски перезагрузим страницу
                window.location.href = window.location.href;
            }
            else
            {
                // в противном случае в переменной response будет текст ошибки
                // и его нужно где-то отобразить
                $('.modalRegError').html( response['MESSAGE'] ).show();
            }           
        };
 
        // явно указываем тип возвращаемых данных
        var responseType = 'html';
 
        // делаем ajax-запрос
        $.post( path, formData, success, responseType );
 
        return false; // не даем форме отправиться обычным способом
    });

});
</script>

</body>

</html>