<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>


<?$counter = 1;?>

							<?foreach($arResult['ELEMENTS'] as $arItem):continue;?>
								
								
								<a class="main-product-item item-<?=$counter?> <?=$counter==1?'active':''?>" href="#" data-slide="<?=$counter?>">
									<div class="main-product-item__caption">REVIT</div>
									<div class="main-product-item__about"><?=$arItem['NAME']?></div>
								</a>
								<?$counter++;?>
							<?endforeach;?>

<?$counter = 1;?>
									<?foreach($arResult['ELEMENTS'] as $arItem):continue;?>

										<div class="swiper-slide" >
										<div class="main-product-desc-item <?$counter==1?'active':''?>">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption"><?=$arItem['NAME']?></div>
											<div class="main-product-desc-item__about"><?=$arItem['PREVIEW_TEXT']?></div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="/software/<?=$arItem['CODE'].'.php'?>"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>


									<?$counter++;?>

									<?endforeach;?>


<section class="main-services-section">
		<div class="container">
			<div class="main-services-list">

				<?$counter = 1;?>

				<?foreach($arResult['ELEMENTS'] as $arItem):?>
								

								<a class="main-service-item" href="/software/<?=$arItem['CODE'].'.php'?>" data-mh="main-service-item">
									<div class="main-service-item__pic" style="background-image: url('<?=CFile::GetPath($arItem["PREVIEW_PICTURE"])?>');"></div>
									<div class="main-service-item__caption"><?=$arItem['NAME']?></div>
									<span class="more-btn"></span>
								</a>
								<?$counter++;?>
				<?endforeach;?>


				<!--
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-1.jpg');"></div>
					<div class="main-service-item__caption">Разработка Стандарта Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-2.jpg');"></div>
					<div class="main-service-item__caption">Обучение сотрудников Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-3.jpg');"></div>
					<div class="main-service-item__caption">Организация работы над проектом в программном обеспечении (ПО)</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-4.jpg');"></div>
					<div class="main-service-item__caption">Консультации <nobr>ИТ-отдела</nobr> предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-5.jpg');"></div>
					<div class="main-service-item__caption">Удаленное администрирование информационных систем</div>
					<span class="more-btn"></span>
				</a>
				-->
			</div>
		</div>
	</section>




