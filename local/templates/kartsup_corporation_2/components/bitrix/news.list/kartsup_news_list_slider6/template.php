<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="services-toggle-items">

					<?foreach($arResult['ITEMS'] as $arItem):?>
								<?
									
									$res = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arItem['ID'], "sort", "asc", array("CODE" => "REFERENCE_URL"));
    								if ($ob = $res->GetNext())
   		 							{
        								$href_url = $ob['VALUE'];
									} else {
										$href_url = '#';

									}

									if(!$href_url || $href_url == ''){
										$href_url = '#';
									}
								?>
								<!--
								CFile::GetPath($arItem['DETAIL_PICTURE'])
								<?=SITE_TEMPLATE_PATH?>/pic/project-logo-big-1.png
								-->
								<div class="services-toggle-item wow fadeInUp" name="<?=$arItem['CODE']?>">
						
									<div class="services-toggle-item-head">
										<div class="services-toggle-item-head__pic"><img src="<?=CFile::GetPath($arItem['DETAIL_PICTURE']['ID'])?>" alt=""></div>
										<div class="services-toggle-item-head__content" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/portfolio-content-bg-1.jpg');">
											<div class="services-toggle-item-head__caption"><?=$arItem['NAME']?></div>
											<a class="more-btn white-btn toggle-btn" href="<?=$href_url;?>"></a>
										</div>
									</div>
						
									<div class="services-toggle-item-body">
										<div class="services-toggle-item-body__box b-content">
											<?=htmlspecialcharsBack($arItem['PREVIEW_TEXT'])?>
											<!--
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda atque consectetur dolores doloribus est explicabo facere incidunt, nam nobis nostrum quibusdam quos, similique sint temporibus ullam veniam, veritatis voluptas.</p>
											<ul>
												<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea facilis iusto laudantium, odit perspiciatis quam quis quisquam tempore ut! Consequuntur deleniti eius iure nemo perspiciatis quasi reprehenderit, rerum sit sunt.</li>
												<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea facilis iusto laudantium, odit perspiciatis quam quis quisquam tempore ut! Consequuntur deleniti eius iure nemo perspiciatis quasi reprehenderit, rerum sit sunt.</li>
											</ul>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda cupiditate dicta eum maiores repellendus suscipit ut! Blanditiis dolorem, hic id iure maiores, molestias odit sint tempora ullam velit voluptates?</p>
											-->
										</div>
									</div>
						
								</div>
								<!--
								<div class="col-md-3 col-sm-6 col-xs-12 project-col">
									<a class="project-item" href="<?=$href_url;?>">
										<div class="project-item__logo">
											<img src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE'])?>" alt="">
										</div>
										<div class="project-item__back">
											<div class="project-item__name"><?=$arItem['NAME']?></div>
											<div class="project-item__more more-btn"></div>
										</div>
									</a>
								</div>
								-->

							<?endforeach;?>
</div>
<script type="text/javascript">
	//var services-toggle-item;

	var item = "<?=isset($_GET['item'])?$_GET['item']:'null'?>";

	$(document).ready(function(){
		if(item !== null && $('.services-toggle-item[name="'+item+'"]').length){
			$('html, body').animate({
                    scrollTop: $('.services-toggle-item[name="'+item+'"]').offset().top
                }, 1000);
			$('.services-toggle-item[name="'+item+'"] .services-toggle-item-body').css('display','block');
		}

	});
</script>
