<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
							<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>
							<a class="news-item" href="<?=$arItem["DETAIL_PAGE_URL"]?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-ajax-element-id="<?=$arItem['ID']?>">
								<div class="news-item__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></div>
								<div class="news-item__caption"><?echo $arItem["NAME"]?></div>
								<div class="news-item__preview"><?echo $arItem["PREVIEW_TEXT"];?></div>
							</a>
						<?endforeach?>