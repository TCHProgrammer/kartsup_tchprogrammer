<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>

<section class="main-slider-section">
		<div class="container">
			<div class="main-slider-section__box clearfix">
				<div class="main-products-list">
					<div class="main-products-list__box">
						<h2>Наши программные продукты</h2>
						<a class="more-btn white-btn" href="#"></a>
						<div class="main-slider-bullets"></div>
						<div class="main-products-items">
							<?$counter = 1;?>

							<?foreach($arResult['ELEMENTS'] as $arItem):?>
								
								
								<a class="main-product-item item-<?=$counter?> <?=$counter==1?'active':''?>" href="#" data-slide="<?=$counter?>">
									<div class="main-product-item__caption">REVIT</div>
									<div class="main-product-item__about"><?=$arItem['NAME']?></div>
								</a>
								<?$counter++;?>
							<?endforeach;?>
							<!--
							<a class="main-product-item item-1 active" href="#" data-slide="1">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Копирование параметров</div>
							</a>
							<a class="main-product-item item-2" href="#" data-slide="2">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Дополнение редактирования</div>
							</a>
							<a class="main-product-item item-3" href="#" data-slide="3">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Менеджер листов</div>
							</a>
							<a class="main-product-item item-4" href="#" data-slide="4">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Рабочая плоскость</div>
							</a>
							<a class="main-product-item item-5" href="#" data-slide="5">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Дополнение Переименовка</div>
							</a>
							<a class="main-product-item item-6" href="#" data-slide="6">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Revit UI reset</div>
							</a>
							<a class="main-product-item item-7" href="#" data-slide="7">
								<div class="main-product-item__caption">REVIT</div>
								<div class="main-product-item__about">Суперфильтр</div>
							</a>
							-->
						</div>
					</div>
				</div>
				<div class="main-product-desc">
					<div class="main-product-desc__box">
						<div class="main-product-desc-slider__wrap">
							<div id="mainProductDescSlider" class="main-product-desc-slider swiper-container">
								<div class="swiper-wrapper">
									<?$counter = 1;?>
									<?foreach($arResult['ELEMENTS'] as $arItem):?>

										<div class="swiper-slide" >
										<div class="main-product-desc-item <?$counter==1?'active':''?>">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption"><?=$arItem['NAME']?></div>
											<div class="main-product-desc-item__about"><?=$arItem['PREVIEW_TEXT']?></div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="/software/<?=$arItem['CODE'].'.php'?>"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>


									<?$counter++;?>

									<?endforeach;?>
									<!--
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Копирование параметров</div>
											<div class="main-product-desc-item__about">Стандартный инструмент «Сопоставление свойств типа» не всегда работает так, как хотелось бы. В большинстве случаев он передает данные Типа семейства и лишь некоторые параметры. Данное дополнение позволяет избавиться от этих трудностей.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Дополнение редактирования</div>
											<div class="main-product-desc-item__about">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sit amet est efficitur, auctor odio quis, placerat enim. Donec quis ligula in enim rhoncus gravida</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Менеджер листов</div>
											<div class="main-product-desc-item__about">Nullam ut condimentum massa. Nulla maximus magna nec cursus viverra. Suspendisse scelerisque suscipit iaculis Nullam ut condimentum massa. Nulla maximus magna nec cursus viverra. Suspendisse scelerisque suscipit iaculis</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Рабочая плоскость</div>
											<div class="main-product-desc-item__about">Fusce venenatis mi at nibh tristique, sed rhoncus dui feugiat. Nunc gravida sollicitudin dui a malesuada. Fusce venenatis mi at nibh tristique, sed rhoncus dui feugiat. Nunc gravida sollicitudin dui a malesuada.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Дополнение Переименовка</div>
											<div class="main-product-desc-item__about">Duis tincidunt diam sollicitudin ullamcorper tristique. In purus lacus, viverra eget diam sit amet, ornare varius nisi. Duis tincidunt diam sollicitudin ullamcorper tristique. In purus lacus, viverra eget diam sit amet, ornare varius nisi.</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Revit UI reset</div>
											<div class="main-product-desc-item__about">Aliquam aliquet interdum leo eu tempor. Duis faucibus bibendum varius. Pellentesque porttitor libero dui, in auctor felis fermentum id Aliquam aliquet interdum leo eu tempor. Duis faucibus bibendum varius. Pellentesque porttitor libero dui, in auctor felis fermentum id</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="main-product-desc-item">
											<div class="main-product-desc-item__logo">
												<img src="<?=SITE_TEMPLATE_PATH?>/pic/revit-logo.png" alt="">
											</div>
											<div class="main-product-desc-item__caption">Суперфильтр</div>
											<div class="main-product-desc-item__about">Donec volutpat enim nec est convallis tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus ac mauris vel tellus maximus luctus Donec volutpat enim nec est convallis tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus ac mauris vel tellus maximus luctus</div>
											<div class="main-product-desc-item__more"><a class="arrow-btn white-btn" href="#"><span>Подробная информация</span><i></i></a></div>
										</div>
									</div>
									-->
								</div>
							</div>
						</div>
						
						<div class="main-product-desc-controls">
							<div class="prev-project-slide"></div>
							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>
							<div class="next-project-slide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>




