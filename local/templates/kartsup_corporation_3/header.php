<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

IncludeTemplateLangFile(__FILE__);

?>

<!DOCTYPE html>

<html class="no-js" lang="ru" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">

<head>


	<?$APPLICATION->ShowHead();?>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<title>KartsUP. Программные продукты. REVIT Копирование параметров</title>
	
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
	<meta name="format-detection" content="telephone=no">
	
	<link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" type="image/x-icon">
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/template_styles.css">

	<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/jquery.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/modernizr.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/css_browser_selector.js"></script>

</head>

<body>

<div id="administrating_panel">

	<?$APPLICATION->ShowPanel();?>

	</div>

<!-- Responsive Nav -->
<div class="responsive-nav-wrap regular-responsive-nav-wrap">
	<div class="responsive-nav-box">
		<div class="container">
			<nav class="responsive-nav">

				<ul>
					<li class="current"><a href="#">Программные продукты</a></li>
					<li><a href="#">Разработка ПО</a></li>
					<li><a href="#">BIM-менеджмент</a></li>
					<li><a href="#">Новости</a></li>
					<li><a href="#">Портфолио</a></li>
					<li><a href="#">О компании</a></li>
					<li><a href="#">Контакты</a></li>
				</ul>


			</nav>
			<div class="responsive-nav-btns visible-xs">
				<a class="login-link" data-fancybox data-src="#loginModal" href="#">Войти</a>
				<a class="reg-link" data-fancybox data-src="#regModal" href="#">Регистрация</a>
				<a class="lang-link" href="#">EN</a>
			</div>
		</div>
	</div>
</div>
<div class="responsive-nav-overlay"></div>
<!-- / Responsive Nav -->

<div class="page-wrapper regular-page-wrapper">
	
	<header class="regular-header">
		<div class="container">
			<div class="regular-header__box clearfix">
				<div class="regular-header__logo">
					<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/pic/regular-logo.png" alt="KartsUP. Разработка программного обеспечения"></a>
				</div>
				<div class="regular-header__right">
					<nav class="regular-header__nav">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "kartsup_horizontal_header_menu", Array(
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
							"MAX_LEVEL" => "3",	// Уровень вложенности меню
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
						),
							false
						);
?>
					</nav>
					<?if(!$USER->IsAuthorized()):?>
					<div class="regular-header__auth">
						<a class="login-btn" data-fancybox data-src="#loginModal" href="#"><span>Войти</span><i></i></a>
						<a class="registration-btn" data-fancybox data-src="#regModal" href="#"><span>Регистрация</span><i></i></a>
						<a class="lang-switcher" href="#">EN</a>
					</div>
					<?else:?>
					<div class="regular-header__auth">
						<a class="entry-lk-link" href="/personal/profile.php">Личный кабинет</a>
						<a class="lang-switcher" href="#">EN</a>
					</div>
					<?endif;?>
					<div class="regular-header__resp">
						<div class="page-name">Программные продукты</div>
						<div class="hamburger hamburger--3dx open-resp-nav">
							<div class="hamburger-box">
								<div class="hamburger-inner"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<section class="products-head-section">
		<div class="container">
			<h1>Программные продукты</h1>
			<div class="prod-slider-bullets"></div>
			<div class="top-products-slider-box">
				<div id="topProductsSlider" class="top-products-slider swiper-container">
					<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"kartsup_horizontal_header_menu_2", 
	array(
		"ROOT_MENU_TYPE" => "leftfirst",
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "kartsup_horizontal_header_menu_2",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	),
	false
);
					?>
					<!--
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<a class="products-head-item product-1 current" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Копирование параметров</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-2" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Дополнение редактирования</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-3" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Менеджер листов</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-4" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Рабочая плоскость</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-5" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Дополнение Переименовка</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-6" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Revit UI reset</div>
							</a>
						</div>
						<div class="swiper-slide">
							<a class="products-head-item product-7" href="#">
								<div class="products-head-item__caption">REVIT</div>
								<div class="products-head-item__about">Суперфильтр</div>
							</a>
						</div>
					</div>
					-->
				</div>
				<div class="top-products-slider-nav">
					<div class="prev-product-slide"></div>
					<div class="next-product-slide"></div>
				</div>
			</div>
		</div>
	</section>