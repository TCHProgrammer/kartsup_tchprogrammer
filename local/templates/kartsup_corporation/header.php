<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>

<!DOCTYPE html>



<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">

<head>
	
	

	<?$APPLICATION->ShowHead();?>

	<title><?$APPLICATION->ShowTitle()?></title>

	<? CJSCore::Init(array("jquery")); ?>


	
	

	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
	
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,700,900&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/template_styles.css">
	
	<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/modernizr.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/libs/css_browser_selector.js"></script>

</head>

<body>
	<div id="administrating_panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
<!-- Responsive Nav -->
<div class="responsive-nav-wrap">
	<div class="responsive-nav-box">
		<div class="container">
			<nav class="responsive-nav">
				<ul>
					<li><a href="#">Программные продукты</a></li>
					<li><a href="#">Разработка ПО</a></li>
					<li><a href="#">BIM-менеджмент</a></li>
					<li><a href="#">Новости</a></li>
					<li><a href="#">Портфолио</a></li>
					<li><a href="#">О компании</a></li>
					<li><a href="#">Контакты</a></li>
				</ul>
			</nav>
			<div class="responsive-nav-btns visible-xs">
				<a class="login-link" data-fancybox data-src="#loginModal" href="#">Войти</a>
				<a class="reg-link" data-fancybox data-src="#regModal" href="#">Регистрация</a>
				<a class="lang-link" href="#">EN</a>
			</div>
		</div>
	</div>
</div>
<div class="responsive-nav-overlay"></div>

<div class="page-wrapper main-page-wrapper">
	
	<section class="main-page-top-section">
		<div class="container">
			<div class="main-page-top-section__box">
				<header class="main-page-header clearfix">
					<div class="main-page-header__logo">
						<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/pic/logo.png" alt="KartsUP. Разработка программного обеспечения"></a>
					</div>

					<nav class="main-page-header__nav">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "kartsup_horizontal_header_menu", Array(
						"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
						"MAX_LEVEL" => "3",	// Уровень вложенности меню
						"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
						"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						"MENU_CACHE_TYPE" => "A",	// Тип кеширования
						"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
						"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
						"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					),
					false
				);
					?>
						<!--
						<ul>
							<li><a href="#">Программные продукты</a></li>
							<li><a href="#">Разработка ПО</a></li>
							<li><a href="#">BIM-менеджмент</a></li>
							<li><a href="#">Новости</a></li>
							<li><a href="#">Портфолио</a></li>
							<li><a href="#">О компании</a></li>
							<li><a href="#">Контакты</a></li>
						</ul>
						-->
					</nav>


					<div class="hamburger hamburger--3dx open-resp-nav">
						<div class="hamburger-box">
							<div class="hamburger-inner"></div>
						</div>
					</div>
				</header>
				<div class="main-page-top-phone"><a href="tel:+79038443648">+7(903)-844-36-48</a></div>
				<h1>
					<span>Разработка программного обеспечения</span>
					<span>Удаленное администрирование всех информационных систем</span>
					<span>BIM-менеджмент для проектных организаций</span>
				</h1>
				<?if(!$USER->IsAuthorized()):?>
				<div class="main-page-top-auth clearfix">
					<div class="main-page-top-auth__btns">
						<a class="lang-switcher" href="#">EN</a>
						<a class="arrow-btn login-btn" data-fancybox data-src="#loginModal" href="#"><span>Войти</span><i></i></a>
						<a class="arrow-btn registration-btn" data-fancybox data-src="#regModal" href="#"><span>Регистрация</span><i></i></a>
					</div>
				</div>
				<?else:?>
				<div class="main-page-top-auth user-authorized clearfix">
					<div class="main-page-top-auth__btns">
						<a class="lang-switcher" href="#">EN</a>
						<a class="entry-lk-link" href="/personal/profile.php">Личный кабинет</a>
					</div>
				</div>
				<?endif;?>
			</div>
		</div>
	</section>