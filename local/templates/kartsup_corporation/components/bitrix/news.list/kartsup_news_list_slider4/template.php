<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="services-toggle-items">

	<?foreach($arResult['ITEMS'] as $arItem):?>

			<div class="services-toggle-item wow fadeInUp">
						
						<div class="services-toggle-item-head">
							<div class="services-toggle-item-head__pic" style="background-image: url('<?=CFile::GetPath($arItem['PREVIEW_PICTURE']['ID'])?>');"></div>
							<div class="services-toggle-item-head__content" style="background-image: url('<?=CFile::GetPath($arItem['DETAIL_PICTURE']['ID'])?>');">
								<div class="services-toggle-item-head__caption"><?=$arItem['NAME']?></div>
								<a class="more-btn white-btn toggle-btn" href="#"></a>
							</div>
						</div>
						
						<div class="services-toggle-item-body">
							<div class="services-toggle-item-body__box b-content">
								<?=htmlspecialcharsBack($arItem['PREVIEW_TEXT'])?>

							</div>
						</div>
						
					</div>

	<?endforeach;?>				
</div>			
