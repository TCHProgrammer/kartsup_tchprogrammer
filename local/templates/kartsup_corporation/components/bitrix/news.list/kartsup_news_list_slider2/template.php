<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="main-services-section">
		<div class="container">
			<div class="main-services-list">

				<?$counter = 1;?>

				<?foreach($arResult['ITEMS'] as $arItem):?>
								
								<a class="main-service-item" href="/bim-management/?item=<?=$arItem['CODE']?>" data-mh="main-service-item">
									<div class="main-service-item__pic" style="background-image: url('<?=CFile::GetPath($arItem["PREVIEW_PICTURE"]["ID"])?>');"></div>
									<div class="main-service-item__caption"><?=$arItem['NAME']?></div>
									<span class="more-btn"></span>
								</a>
								<?$counter++;?>
				<?endforeach;?>


				<!--
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-1.jpg');"></div>
					<div class="main-service-item__caption">Разработка Стандарта Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-2.jpg');"></div>
					<div class="main-service-item__caption">Обучение сотрудников Предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-3.jpg');"></div>
					<div class="main-service-item__caption">Организация работы над проектом в программном обеспечении (ПО)</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-4.jpg');"></div>
					<div class="main-service-item__caption">Консультации <nobr>ИТ-отдела</nobr> предприятия</div>
					<span class="more-btn"></span>
				</a>
				<a class="main-service-item" href="#" data-mh="main-service-item">
					<div class="main-service-item__pic" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/pic/main-service-pic-5.jpg');"></div>
					<div class="main-service-item__caption">Удаленное администрирование информационных систем</div>
					<span class="more-btn"></span>
				</a>
				-->
			</div>
		</div>
	</section>
