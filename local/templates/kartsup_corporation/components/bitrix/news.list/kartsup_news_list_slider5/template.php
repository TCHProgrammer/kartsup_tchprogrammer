<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="products-slider-section">

					<h2>Программные продукты</h2>

					<div class="products-slider-box">

						<div id="productsSlider" class="products-slider swiper-container">

							<div class="swiper-wrapper">


							<?$counter = 1;?>
									<?foreach($arResult['ITEMS'] as $arItem):?>

								<?if($arItem['CODE']=='index'){$arItem['CODE']='';$ext_href = '';} else {$ext_href = '.php';}?>

								<div class="swiper-slide">

									<a class="products-slide-item" href="/software/<?=$arItem['CODE'].$ext_href?>">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=CFile::GetPath($arItem["PREVIEW_PICTURE"]['ID'])?>" srcset="<?=CFile::GetPath($arItem["PREVIEW_PICTURE"]['ID'])?> 1x, <?=CFile::GetPath($arItem["DETAIL_PICTURE"]['ID'])?> 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT <?=$arItem['NAME']?></div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<?$counter++;?>

									<?endforeach;?>

								<!--

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-2.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-2@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Дополнение редактирования</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-3.png 1x, pic/product-icon-3@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Менеджер листов</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-4.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-4@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Рабочая плоскость</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-5.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-5@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Дополнение Переименовка</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-6.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-6@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT UI reset</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>

								<div class="swiper-slide">

									<a class="products-slide-item" href="#">

										<div class="products-slide-item__head">

											<div class="products-slide-item__icon">

												<img src="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png" srcset="<?=SITE_TEMPLATE_PATH?>/pic/product-icon-7.png 1x, <?=SITE_TEMPLATE_PATH?>/pic/product-icon-7@2x.png 2x" alt="">

											</div>

										</div>

										<div class="products-slide-item__body">

											<div class="products-slide-item__caption">REVIT Суперфильтр</div>

										</div>

										<span class="more-btn"></span>

									</a>

								</div>
								-->

							</div>

						</div>


						<div class="products-slider-controls">

							<div class="prev-project-slide"></div>

							<div class="slide-counter"><span class="current">1</span>/<span class="total"></span></div>

							<div class="next-project-slide"></div>

						</div>

					</div>

				</section>
