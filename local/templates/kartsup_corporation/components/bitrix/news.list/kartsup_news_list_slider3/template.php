<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="projects-list row">


							<?$counter = 1;?>

							<?foreach($arResult['ITEMS'] as $arItem):?>
								<?
	/*
									$res = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arItem['ID'], "sort", "asc", array("CODE" => "REFERENCE_URL"));
    								if ($ob = $res->GetNext())
   		 							{
        								$href_url = $ob['VALUE'];
									} else {
										$href_url = '#';

									}

									if(!$href_url || $href_url == ''){
										$href_url = '#';
									}
*/
								?>
								
								<div class="col-md-3 col-sm-6 col-xs-12 project-col">
									<a class="project-item" href="<?='/portfolio/?item='.$arItem['CODE'];?>">
										<div class="project-item__logo">
											<img src="<?=CFile::GetPath($arItem['PREVIEW_PICTURE']['ID'])?>" alt="">
										</div>
										<div class="project-item__back">
											<div class="project-item__name"><?=$arItem['NAME']?></div>
											<div class="project-item__more more-btn"></div>
										</div>
									</a>
								</div>

							<?endforeach;?>

						
</div>
