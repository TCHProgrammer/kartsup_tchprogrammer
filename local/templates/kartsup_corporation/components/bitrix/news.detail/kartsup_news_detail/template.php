<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="news-detail-box">
					
					<div class="news-detail-cols clearfix">
						<div class="news-detail-main">
							<div class="news-detail-content">
								<div class="news-detail-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
								<div class="news-detail-caption"><?=$arResult["NAME"]?></div>
								<div class="news-detail-text b-content">
									<?echo $arResult["DETAIL_TEXT"];?>
								</div>
							</div>
						</div>
						<aside class="news-detail-aside">
							<div class="news-detail-btns">
<!--
								<?
								$from_sub_dir = 'news';
								$files_array = array(
									"REVIT1.txt",
									"REVIT2.txt",
									"REVIT3.txt",
								);

								function extractNameFromFileName($fileName){
									$exp = explode('.',$fileName);
									array_pop($exp);
									return implode($exp);
								}

							?>

							<?foreach($files_array as $file):?>
								<a class="arrow-btn download-btn" href="<?='/downloads/'.$from_sub_dir.'/'.$file?>"><span><?=extractNameFromFileName($file)?></span><s></s></a>
							<?endforeach;?>

-->
								<?

									downloads::displayDownloadReferencesFromNewsId($arResult['ID']);
									downloads::displayDownloadReferencesFromNewsFiles($arResult['ID'])
								?>

								<!--

								<a class="arrow-btn download-btn" href="#"><span>Менеджер листов 2018</span><s></s></a>
								<a class="arrow-btn download-btn" href="#"><span>Суперфильтр</span><s></s></a>
								<a class="arrow-btn download-btn" href="#"><span>Копирование параметров</span><s></s></a>
								-->
							</div>
						</aside>
					</div>

					<div class="news-detail-comments">
						<div class="add-comment-box">
							<div class="add-comment-avatar"><img src="pic/no-avatar-cap.png" alt=""></div>
							<div class="add-comment-name">Random 007</div>
							<div class="add-comment-btn"><a data-fancybox data-src="#addCommentModal" href="#">Оставить комментарий</a></div>
						</div>
						<div class="news-comments-list">
							<div class="news-comment-item">
								<div class="news-comment-question">
									<div class="news-comment-question__avatar"><img src="pic/no-avatar-cap.png" alt=""></div>
									<div class="news-comment-question__date">02. 04. 2018</div>
									<div class="news-comment-question__name">Random 007</div>
									<div class="news-comment-question__text">Как получить в рамках данной услуги разработанное специализированное программное обеспечение, позволяющее упростить различные рабочие процессы?</div>
								</div>
								<div class="news-comment-answer">
									<div class="news-comment-answer__box">
										<div class="news-comment-answer__avatar"><img src="pic/no-avatar-cap.png" alt=""></div>
										<div class="news-comment-answer__date">02. 04. 2018</div>
										<div class="news-comment-answer__name"><span>001</span><i></i><span>Random 007</span></div>
										<div class="news-comment-answer__text">За подробностями сюда <a href="mailto:kart@kartsup.ru">kart@kartsup.ru</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<a class="current-news-close" href="#" title="Закрыть новость"></a>
					
				</div>

<!--
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
-->